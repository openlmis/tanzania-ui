/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('RequisitionViewController', function() {

    beforeEach(function() {
        var context = this;
        module('requisition-view', function($provide) {
            context.RequisitionStockCountDateModalMock = jasmine.createSpy('RequisitionStockCountDateModal');

            $provide.factory('RequisitionStockCountDateModal', function() {
                return context.RequisitionStockCountDateModalMock;
            });
        });
        module('referencedata-facility-type-approved-product');
        module('referencedata-facility');
        module('referencedata-program');
        module('referencedata-period');

        var RequisitionDataBuilder, RequisitionLineItemDataBuilder, ProgramDataBuilder;
        inject(function($injector) {
            RequisitionDataBuilder = $injector.get('RequisitionDataBuilder');
            ProgramDataBuilder = $injector.get('ProgramDataBuilder');
            RequisitionLineItemDataBuilder = $injector.get('RequisitionLineItemDataBuilder');
            this.FacilityDataBuilder = $injector.get('FacilityDataBuilder');
            this.PeriodDataBuilder = $injector.get('PeriodDataBuilder');

            this.$rootScope = $injector.get('$rootScope');
            this.$scope = this.$rootScope.$new();
            this.$state = $injector.get('$state');
            this.$q = $injector.get('$q');
            this.$window = $injector.get('$window');
            this.notificationService = $injector.get('notificationService');
            this.alertService = $injector.get('alertService');
            this.confirmService = $injector.get('confirmService');
            this.loadingModalService = $injector.get('loadingModalService');
            this.stateTrackerService = $injector.get('stateTrackerService');
            this.messageService = $injector.get('messageService');
            this.requisitionUrlFactory = $injector.get('requisitionUrlFactory');
            this.$controller = $injector.get('$controller');
            this.requisitionValidator = $injector.get('requisitionValidator');
            this.RequisitionStockCountDateModal = $injector.get('RequisitionStockCountDateModal');
            this.authorizationService = $injector.get('authorizationService');
            this.RequisitionWatcher = $injector.get('RequisitionWatcher');
            this.accessTokenFactory = $injector.get('accessTokenFactory');
            this.requisitionService = $injector.get('requisitionService');
            this.offlineService = $injector.get('offlineService');
            this.facilityService = $injector.get('facilityService');
            this.programService = $injector.get('programService');
            this.periodService = $injector.get('periodService');
        });

        this.program = new ProgramDataBuilder()
            .withEnabledDatePhysicalStockCountCompleted()
            .build();

        this.facility = new this.FacilityDataBuilder().build();
        this.period = new this.PeriodDataBuilder().build();
        this.requisition = new RequisitionDataBuilder()
            .withProgram(this.program)
            .withRequisitionLineItems([
                new RequisitionLineItemDataBuilder()
                    .fullSupplyForProgram(this.program)
                    .buildJson(),
                new RequisitionLineItemDataBuilder()
                    .nonFullSupplyForProgram(this.program)
                    .buildJson()
            ])
            .build();

        this.fullSupplyItems = [this.requisition.requisitionLineItems[0]];
        this.nonFullSupplyItems = [this.requisition.requisitionLineItems[1]];

        this.requisition.$isInitiated.andReturn(true);
        this.requisition.$isReleased.andReturn(false);
        this.requisition.$isRejected.andReturn(false);

        this.canSubmit = true;
        this.canAuthorize = false;
        this.canApproveAndReject = false;
        this.canDelete = true;
        this.canSkip = true;
        this.canSync = true;

        spyOn(this.stateTrackerService, 'goToPreviousState');
        spyOn(this.notificationService, 'success');
        spyOn(this.notificationService, 'error');
        spyOn(this.alertService, 'error');
        spyOn(this.$state, 'go');
        spyOn(this.$state, 'reload');
        spyOn(this.loadingModalService, 'open');
        spyOn(this.loadingModalService, 'close');
        spyOn(this.requisitionValidator, 'areLineItemsValid');
        spyOn(this.requisitionValidator, 'validateRequisition');
        spyOn(this.requisitionValidator, 'areAllLineItemsSkipped');
        spyOn(this.confirmService, 'confirm');
        spyOn(this.confirmService, 'confirmDestroy');
        spyOn(this.authorizationService, 'hasRight');
        spyOn(this.RequisitionWatcher.prototype, 'disableWatcher');
        spyOn(this.accessTokenFactory, 'addAccessToken');
        spyOn(this.offlineService, 'isOffline');
        spyOn(this.requisitionService, 'removeOfflineRequisition');
        spyOn(this.programService, 'getUserPrograms').andReturn(this.$q.resolve(this.program));
        spyOn(this.facilityService, 'get').andReturn(this.$q.resolve(this.facility));
        spyOn(this.periodService, 'get').andReturn(this.$q.resolve(this.period));
        spyOn(this.rejectionReasonModalService, 'open');

        this.initController = initController;
    });

    describe('Sync error handling', function() {

        beforeEach(function() {
            this.verifyReloadOnErrorAndNotificationSent = verifyReloadOnErrorAndNotificationSent;
            this.verifyNoReloadOnError = verifyNoReloadOnError;
            this.initController();
        });

        function verifyReloadOnErrorAndNotificationSent(responseStatus, messageKey) {
            var conflictResponse = {
                status: responseStatus
            };

            this.requisition.$save.andReturn(this.$q.reject(conflictResponse));

            this.vm.syncRnr();
            this.$rootScope.$apply();

            expect(this.notificationService.error).toHaveBeenCalledWith(messageKey);
            expect(this.$state.reload).toHaveBeenCalled();
        }

        function verifyNoReloadOnError(responseStatus) {
            var conflictResponse = {
                status: responseStatus
            };

            this.requisition.$save.andReturn(this.$q.reject(conflictResponse));

            this.vm.syncRnr();
            this.$rootScope.$apply();

            expect(this.alertService.error).toHaveBeenCalledWith('requisitionView.sync.failure');
            expect(this.$state.reload).not.toHaveBeenCalled();
        }
    });

    describe('updateRequisition', function() {

        beforeEach(function() {
            this.initController();
        });

    });

    describe('loadRejectionReasonModal', function() {

        beforeEach(function() {
            this.initController();
        });

    });

    function initController() {
        this.vm = this.$controller('RequisitionViewController', {
            $scope: this.$scope,
            program: this.program,
            facility: this.facility,
            processingPeriod: this.period,
            requisition: this.requisition,
            canSubmit: this.canSubmit,
            canAuthorize: this.canAuthorize,
            canApproveAndReject: this.canApproveAndReject,
            canDelete: this.canDelete,
            canSkip: this.canSkip,
            canSync: this.canSync
        });
        this.vm.$onInit();
    }

});