/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name buq-specialization-list.controller:BuqSpecializationListController
     *
     * @description
     * Controller for managing specializations list screen.
     */
    angular
        .module('buq-specialization-list')
        .controller('BuqSpecializationListController', controller);

    controller.$inject = ['specializations'];

    function controller(specializations) {
        var vm = this;

        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @propertyOf buq-specialization-list.controller:BuqSpecializationListController
         * @type {Array}
         * @name specializations
         *
         * @description
         * The list of all available specializations in the system.
         */
        vm.specializations = undefined;

        /**
         * @ngdoc method
         * @methodOf buq-specialization-list.controller:BuqSpecializationListController
         * @name $onInit
         *
         * @description
         * Initialization method of the SpecializationListController.
         */
        function onInit() {
            vm.specializations = specializations;
        }

    }

})();