/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name buq-specialization.BuqSpecialization
     *
     * @description
     * Represents a single BuqSpecialization.
     */
    angular
        .module('buq-specialization')
        .factory('BuqSpecialization', BuqSpecialization);

    function BuqSpecialization() {

        return BuqSpecialization;

        /**
         * @ngdoc method
         * @methodOf buq-specialization.BuqSpecialization
         * @name BuqSpecialization
         *
         * @description
         * Creates a new instance of the BuqSpecialization class.
         *
         * @param  {Object}                json       the JSON representation of the
         *     BuqSpecialization
         * @return {BuqSpecialization}                           the BuqSpecialization object
         */
        function BuqSpecialization(json) {
            if (!json) {
                json = {};
            }

            this.id = json.id;
            this.name = json.name;
            this.code = json.code;
            this.description = json.description;
            this.orderableIds = json.orderableIds ? json.orderableIds : [];
            this.facilityIds = json.facilityIds ? json.facilityIds : [];
            this.orderables = json.orderables ? json.orderables : [];
            this.facilities = json.facilities ? json.facilities : [];
        }

    }

})();
