/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name buq-specialization.BuqSpecializationService
     *
     * @description
     * Responsible for retrieving all buq specialization information from server.
     */
    angular
        .module('buq-specialization')
        .service('buqSpecializationService', service);

    service.$inject = [
        '$resource', 'referencedataUrlFactory'
    ];

    function service($resource, referencedataUrlFactory) {

        var resource = $resource(referencedataUrlFactory('/api/specializations/:id'), {}, {
            query: {
                url: referencedataUrlFactory('/api/specializations'),
                method: 'GET',
                isArray: false
            },
            update: {
                url: referencedataUrlFactory('/api/specializations/:id'),
                method: 'PUT'
            }
        });

        this.get = get;
        this.query = query;
        this.update = update;
        this.create = create;
        this.save = save;

        /**
         * @ngdoc method
         * @methodOf buq-specialization.BuqSpecializationService
         * @name get
         *
         * @description
         * Retrieves buq specialization by id.
         *
         * @param  {String}  buqSpecializationId buqSpecialization UUID
         * @return {Promise}                buqSpecialization promise
         */
        function get(buqSpecializationId) {
            return resource.get({
                id: buqSpecializationId
            }).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization.BuqSpecializationService
         * @name query
         *
         * @description
         * Retrieves all buq specializations by ids.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Page of buq specializations
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization.BuqSpecializationService
         * @name update
         *
         * @description
         * Updates existing buq specialization.
         *
         * @param  {Object}  buqSpecialization buq specialization that will be saved
         * @return {Promise}              updated buq specialization
         */
        function update(buqSpecialization) {
            return resource.update({
                id: buqSpecialization.id
            }, buqSpecialization).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization.BuqSpecializationService
         * @name create
         *
         * @description
         * Creates new buq specialization.
         *
         * @param  {Object}  buqSpecialization buq specialization that will be created
         * @return {Promise}              created buq specialization
         */
        function create(buqSpecialization) {
            return resource.save(buqSpecialization).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization.BuqSpecializationService
         * @name save
         *
         * @description
         * Saves buqSpecialization in the repository.
         *
         * @param  {Object}  buqSpecialization buq specialization that will be created
         * @return {Promise} the promise resolving to saved BuqSpecialization, rejected if save was unsuccessful
         */
        function save(buqSpecialization) {
            if (buqSpecialization.id) {
                return update(buqSpecialization);
            }
            return create(buqSpecialization);
        }
    }
})();
