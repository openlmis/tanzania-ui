/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name user-stock-out-notification-list.StockOutNotificationService
     *
     * @description
     * Responsible for retrieving all stock out notification information from server.
     */
    angular
        .module('user-stock-out-notification-list')
        .service('stockOutNotificationService', service);

    service.$inject = ['$resource', 'openlmisUrlFactory',
        'authorizationService'];

    function service($resource, openlmisUrlFactory, authorizationService) {

        var resource = $resource(openlmisUrlFactory('/api'),
            {}, {
                query: {
                    url: openlmisUrlFactory('/api/stock-out-notifications'),
                    method: 'GET',
                    isArray: false
                },
                count: {
                    url: openlmisUrlFactory(
                        '/api/stock-out-notifications/count'),
                    method: 'GET',
                    isArray: false
                }
            });

        this.query = query;
        this.queryUserNotifications = queryUserNotifications;
        this.countUserNotifications = countUserNotifications;

        /**
         * @ngdoc method
         * @methodOf user-stock-out-notification-list.StockOutNotificationService
         * @name query
         *
         * @description
         * Retrieves all stock out notifications.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} List of stock out notification
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf user-stock-out-notification-list.StockOutNotificationService
         * @name queryUserNotifications
         *
         * @description
         * Retrieves user stock out notifications.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} List of user stock out notification
         */
        function queryUserNotifications(queryParams) {
            var params = queryParams || {};
            params = angular.copy(params);

            var user = authorizationService.getUser();
            params.userId = user.user_id;
            return resource.query(params).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf user-stock-out-notification-list.StockOutNotificationService
         * @name count
         *
         * @description
         * Count all stock out notifications.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Count of stock out notification
         */
        function countUserNotifications(queryParams) {
            var params = queryParams || {};
            params = angular.copy(params);

            var user = authorizationService.getUser();
            params.userId = user.user_id;
            return resource.count(params).$promise;
        }

    }
})();
