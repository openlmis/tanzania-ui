/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

  'use strict';

  /**
   * @ngdoc controller
   * @name user-stock-out-notification-list.controller:UserStockOutNotificationListController
   *
   * @description
   * Controller for managing stock out notification list screen.
   */
  angular
  .module('user-stock-out-notification-list')
  .controller('UserStockOutNotificationListController', controller);

  controller.$inject = ['$state', '$stateParams', '$q',
    'openlmisUrlFactory', 'userStockOutNotifications'];

  function controller($state, $stateParams, $q, openlmisUrlFactory, userStockOutNotifications) {
    var vm = this;

    vm.$onInit = onInit;
    vm.search = search;
    vm.getPrintUrl = getPrintUrl;

    /**
     * @ngdoc property
     * @propertyOf user-stock-out-notification-list.controller:UserStockOutNotificationListController
     * @type {Array}
     * @name userStockOutNotifications
     *
     * @description
     * The list of all pending stock out notifications in the system.
     */
    vm.userStockOutNotifications = undefined;

    /**
     * @ngdoc property
     * @propertyOf user-stock-out-notification-list.controller:UserStockOutNotificationListController
     * @name facilityName
     * @type {String}
     *
     * @description
     * Contains name param for searching facilities.
     */
    vm.facilityName = undefined;

    /**
     * @ngdoc method
     * @methodOf user-stock-out-notification-list.controller:UserStockOutNotificationListController
     * @name $onInit
     *
     * @description
     * Initialization method of the UserStockOutNotificationListController.
     */
    function onInit() {
      vm.userStockOutNotifications = userStockOutNotifications;
      vm.facilityName = $stateParams.facilityName;
    }

    /**
     * @ngdoc method
     * @methodOf user-stock-out-notification-list.controller:UserStockOutNotificationListController
     * @name search
     *
     * @description
     * Reloads page with new search parameters.
     */
    function search() {
      var stateParams = angular.copy($stateParams);

      stateParams.facilityName = vm.facilityName;

      $state.go('openlmis.userStockOutNotification', stateParams, {
        reload: true
      });
    }

    /**
     * @ngdoc method
     * @methodOf user-stock-out-notification-list.controller:UserStockOutNotificationListController
     * @name getPrintUrl
     *
     * @description
     * Prepares a print URL for the given SO notification.
     *
     * @param  {Object} notification the SO notification to prepare the URL for
     * @return {String}       the prepared URL
     */
    function getPrintUrl(notification) {
      return openlmisUrlFactory('/api/stock-out-notifications/' + notification.id + '/print');
    }
  }

})();