/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name referencedata-orderable.orderableService
     *
     * @description
     * Responsible for retrieving all processing period information from the server.
     */
    angular
        .module('referencedata-orderable')
        .service('tzOrderableService', service);

    service.$inject = ['$q', '$resource', 'openlmisUrlFactory'];

    function service($q, $resource, openlmisUrlFactory) {

        var resource = $resource(openlmisUrlFactory('/api/msd-orderables/:id'), {}, {

            getOrderableByCode: {
                url: openlmisUrlFactory('/api/msd-orderables/code/:code'),
                method: 'GET',
                isArray: false
            },
            getMsdOrderables: {
                url: openlmisUrlFactory('/api/msd-orderables/all'),
                method: 'GET',
                isArray: true
            },
            search: {
                url: openlmisUrlFactory('/api/orderables'),
                method: 'GET'
            },
            acceptOrderable: {
                url: openlmisUrlFactory('/api/msd-orderables/acceptOrderable'),
                method: 'POST',
                isArray: false
            },
            deleteMsdOrderable: {
                url: openlmisUrlFactory('/api/msd-orderables/delete/:code'),
                method: 'DELETE',
                isArray: false
            }
        });
        this.getOrderableByCode = getOrderableByCode;
        this.acceptOrderable = acceptOrderable;
        this.search = search;
        this.getMsdOrderables = getMsdOrderables;
        this.deleteMsdOrderable = deleteMsdOrderable;

        /**
         * @ngdoc method
         * @name getOrderableByCode
         * @methodOf referencedata-orderable.orderableService
         *
         * @description
         * Delete specified code.
         *
         * @param  {Object}  code code to get.
         * @return {Promise}        msd orderable
         */
        function getOrderableByCode(code) {
            return resource.getOrderableByCode({
                code: code
            }).$promise;
        }

        /**
         * @ngdoc method
         * @name search
         * @methodOf referencedata-orderable.orderableService
         *
         * @description
         * Delete specified code.
         *
         * @param  {Object}  code code to get.
         * @return {Promise}      orderable
         */
        function search(code) {
            return resource.search({
                code: code
            }).$promise;
        }

        /**
         * @ngdoc method
         * @name create
         * @methodOf referencedata-orderable.orderableService
         *
         * @description
         * Creates msd orderables.
         *
         * @param  {Object}  orderable new Orderable
         * @return {Promise}        created Orderable
         */
        function acceptOrderable(orderable) {
            return resource.acceptOrderable(orderable).$promise;
        }

        /**
         * @ngdoc method
         * @name getMsdOrderables
         * @methodOf referencedata-orderable.orderableService
         *
         * @description
         * Get msd orderables.
         *
         * @return {Promise}        get Orderable
         */
        function getMsdOrderables() {
            return resource.getMsdOrderables().$promise;
        }

        /**
         * @ngdoc method
         * @name delete
         * @methodOf referencedata-orderable.orderableService
         *
         * @description
         * Delete specified orderable code.
         *
         * @param  {Object}  code code to delete.
         */
        function deleteMsdOrderable(code) {
            return resource.deleteMsdOrderable({
                code: code
            }).$promise;
        }

    }
})();
