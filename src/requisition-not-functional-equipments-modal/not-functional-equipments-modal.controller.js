/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name requisition-not-functional-equipments-modal:NotFunctionalEquipmentsModalController
     *
     * @description
     * Manages Not Functional Equipments Modal.
     */
    angular
        .module('requisition-not-functional-equipments-modal')
        .controller('NotFunctionalEquipmentsModalController', controller);

    controller.$inject = ['requisition', 'notFunctionalEquipmentProducts', 'modalDeferred'];

    function controller(requisition, notFunctionalEquipmentProducts, modalDeferred) {
        var vm = this;

        vm.$onInit = onInit;
        vm.confirm = confirm;

        /**
         * @ngdoc property
         * @propertyOf requisition-not-functional-equipments-modal:NotFunctionalEquipmentsModalController
         * @name requisition
         * @type {Object}
         *
         * @description
         * Requisition to be submitted.
         */
        vm.requisition = undefined;

        /**
         * @ngdoc property
         * @propertyOf requisition-not-functional-equipments-modal:NotFunctionalEquipmentsModalController
         * @name notFunctionalEquipmentProducts
         * @type {Array}
         *
         * @description
         * A map of requested products and associated equipments which are not functional.
         */
        vm.notFunctionalEquipmentProducts = undefined;

        /**
         * @ngdoc method
         * @methodOf requisition-not-functional-equipments-modal:NotFunctionalEquipmentsModalController
         * @name $onInit
         *
         * @description
         * Initialization method of the NotFunctionalEquipmentsModalController.
         */
        function onInit() {
            vm.requisition = requisition;
            vm.notFunctionalEquipmentProducts = notFunctionalEquipmentProducts;
        }

        /**
         * @ngdoc method
         * @methodOf requisition-not-functional-equipments-modal:NotFunctionalEquipmentsModalController
         * @name confirm
         *
         * @description
         * Confirm added that the process can proceed even with some of equipments not being functional.
         */
        function confirm() {
            modalDeferred.resolve();
        }
    }
})();
