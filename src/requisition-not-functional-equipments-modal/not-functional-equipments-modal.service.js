/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name requisition-not-functional-equipments-modal.notFunctionalEquipmentsModalService
     *
     * @description
     * This service will pop up a modal window for user
     * to confirm submitting requisition with not functional equipment.
     */
    angular
        .module('requisition-not-functional-equipments-modal')
        .service('notFunctionalEquipmentsModalService', service);

    service.$inject = ['openlmisModalService'];

    function service(openlmisModalService) {
        this.show = show;

        /**
         * @ngdoc method
         * @methodOf requisition-not-functional-equipments-modal.notFunctionalEquipmentsModalService
         * @name show
         *
         * @description
         * Shows modal that allows users to choose products.
         *
         * @param  {Array}   availableItems orderable + lot items that can be selected
         * @param  {Array}   selectedItems  orderable + lot items that were added already
         * @return {Promise}                resolved with selected products.
         */
        function show(requisition, notFunctionalEquipmentProducts) {
            return openlmisModalService.createDialog(
                {
                    controller: 'NotFunctionalEquipmentsModalController',
                    controllerAs: 'vm',
                    templateUrl: 'requisition-not-functional-equipments-modal/not-functional-equipments-modal.html',
                    show: true,
                    resolve: {
                        requisition: function() {
                            return requisition;
                        },
                        notFunctionalEquipmentProducts: function() {
                            return notFunctionalEquipmentProducts;
                        }
                    }
                }
            ).promise.finally(function() {
                angular.element('.popover').popover('destroy');
            });
        }
    }

})();
