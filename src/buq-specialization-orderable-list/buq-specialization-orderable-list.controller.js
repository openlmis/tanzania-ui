/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
     *
     * @description
     * Controller for managing buqSpecOrderables list screen.
     */
    angular
        .module('buq-specialization-orderable-list')
        .controller('BuqSpecializationOrderableListController', controller);

    controller.$inject = ['buqSpecialization', 'buqSpecOrderables', 'orderables',
        'buqSpecOrderablesMap', 'selectProductsModalService',
        'buqSpecializationService', 'stateTrackerService'];

    function controller(buqSpecialization, buqSpecOrderables, orderables,
                        buqSpecOrderablesMap, selectProductsModalService,
                        buqSpecializationService, stateTrackerService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.addOrderables = addOrderables;
        vm.removeOrderable = removeOrderable;
        vm.saveBuqSpecialization = saveBuqSpecialization;
        vm.goToBuqSpecializationList = goToBuqSpecializationList;

        /**
         * @ngdoc property
         * @propertyOf buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
         * @type {Array}
         * @name buqSpecOrderables
         *
         * @description
         * The list of all available buqSpecOrderables in the system.
         */
        vm.buqSpecOrderables = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
         * @type {BuqSpecialization}
         * @name buqSpecialization
         *
         * @description
         * BuqSpecialization that is being created.
         */
        vm.buqSpecialization = undefined;

        /**
         * @ngdoc method
         * @methodOf buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
         * @name $onInit
         *
         * @description
         * Initialization method of the BuqSpecializationOrderableListController.
         */
        function onInit() {
            vm.buqSpecOrderables = buqSpecOrderables;
            vm.buqSpecialization = buqSpecialization;
            vm.buqSpecOrderablesMap = buqSpecOrderablesMap;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
         * @name addOrderables
         *
         * @description
         * Method that displays a modal for selecting and adding a orderable to the UI
         */
        function addOrderables() {
            selectProductsModalService.show(
                {
                    products: orderables,
                    selections: vm.buqSpecOrderablesMap
                }
            )
                .then(addToOrderablesMap)
                .then(updateSpecializationOrderables);
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
         * @name removeOrderable
         *
         * @description
         * Method that removes kit constituent from the specialization orderables
         *
         * @param {Object} a single orderable constituent to be removed
         */
        function removeOrderable(orderable) {
            if (vm.buqSpecOrderables.indexOf(orderable) > -1) {
                vm.buqSpecOrderables.splice(
                    vm.buqSpecOrderables.indexOf(orderable), 1
                );
            }

            vm.buqSpecialization.orderables = vm.buqSpecOrderables;

            Object.keys(vm.buqSpecOrderablesMap).forEach(function(key) {
                if (key === orderable.id) {
                    delete vm.buqSpecOrderablesMap[orderable.id];
                }
            });
        }

        function updateSpecializationOrderables(selectedOrderables) {
            while (vm.buqSpecOrderables.length) {
                vm.buqSpecOrderables.pop();
            }
            selectedOrderables.forEach(function(orderable) {
                vm.buqSpecOrderables.push(orderable);
            });

            vm.buqSpecialization.orderables = vm.buqSpecOrderables;

        }

        function addToOrderablesMap(orderables) {
            vm.buqSpecOrderablesMap =
                orderables.reduce(function(orderablesMap, orderable) {
                    orderablesMap[orderable.id] = orderable;
                    return orderablesMap;
                }, {});

            return orderables;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
         * @name goToBuqSpecializationList
         *
         * @description
         * Redirects to buqSpecialization list screen.
         */
        function goToBuqSpecializationList() {
            stateTrackerService.goToPreviousState(
                'openlmis.administration.buqSpecializations', {},
                {
                    reload: true
                }
            );
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-orderable-list.controller:BuqSpecializationOrderableListController
         * @name saveBuqSpecialization
         *
         * @description
         * Updates the buqSpecialization and return to the buqSpecialization list on success.
         */
        function saveBuqSpecialization() {
            vm.buqSpecialization.orderableIds = [];
            vm.buqSpecialization.orderables.forEach(function(orderable) {
                vm.buqSpecialization.orderableIds.push(orderable.id);
            });
            return buqSpecializationService.update(vm.buqSpecialization)
                .then(goToBuqSpecializationList);
        }
    }

})();