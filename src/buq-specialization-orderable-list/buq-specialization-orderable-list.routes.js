/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular.module('buq-specialization-orderable-list').config(routes);

    routes.$inject =
        ['$stateProvider', 'selectProductsModalStateProvider', 'ADMINISTRATION_RIGHTS'];

    function routes($stateProvider, selectProductsModalStateProvider, ADMINISTRATION_RIGHTS) {
        selectProductsModalStateProvider.stateWithAddOrderablesChildState(
            'openlmis.administration.buqSpecializationOrderables', {
                showInNavigation: false,
                label: 'buqSpecializationOrderableList.orderables',
                url: '/specializationOrderables/:id,?page&size',
                controller: 'BuqSpecializationOrderableListController',
                controllerAs: 'vm',
                nonTrackable: true,
                templateUrl: 'buq-specialization-orderable-list/buq-specialization-orderable-list.html',
                accessRights: [ADMINISTRATION_RIGHTS.MANAGE_BUQ],
                resolve: {
                    buqSpecialization: function(
                        BuqSpecialization,
                        buqSpecializationService, $stateParams
                    ) {
                        return buqSpecializationService.get($stateParams.id)
                            .then(function(specialization) {
                                return new BuqSpecialization(specialization);
                            });
                    },
                    buqSpecOrderables: function(
                        paginationService, buqSpecialization, $stateParams
                    ) {
                        return paginationService.registerList(null, $stateParams, function() {
                            return buqSpecialization.orderables;
                        }, {
                            paginationId: 'buqSpecOrderableList'
                        });
                    },
                    buqSpecOrderablesMap: function(buqSpecialization) {
                        return buqSpecialization.orderables.reduce(
                            function(orderablesMap, orderable) {
                                orderablesMap[orderable.id] = orderable;
                                return orderablesMap;
                            }, {}
                        );
                    },
                    orderables: function(OrderableResource) {
                        return new OrderableResource()
                            .query()
                            .then(function(orderables) {
                                return orderables.content;
                            });
                    }
                }
            }
        );
    }
})();
