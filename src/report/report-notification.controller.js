/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name report.controller:ReportNotificationController
     *
     * @description
     * Controller for report options page.
     */
    angular
        .module('report')
        .controller('ReportNotificationController', controller);

    controller.$inject = [
        '$state', '$scope', '$window', 'stockNotifications', 'openlmisUrlFactory', 'accessTokenFactory'
    ];

    function controller($state, $scope, $window, stockNotifications, openlmisUrlFactory, accessTokenFactory) {
        var vm = this;

        vm.print = print;

        /**
         * @ngdoc property
         * @propertyOf report.controller:ReportNotificationController
         * @name stockNotifications
         * @type {Object}
         *
         * @description
         * The object representing the selected report.
         */
        vm.stockNotifications = stockNotifications;

        /**
         * @ngdoc method
         * @methodOf order-view.controller:OrderViewController
         * @name downloadReport
         *
         * @description
         * Prepares a print URL for the given order.
         *
         * @param  {Object} notification the order to prepare the URL for
         * @return {String}       the prepared URL
         */
        function print(notification) {

            $window.open(
                accessTokenFactory.addAccessToken(
                    openlmisUrlFactory('/api/stockOutNotifications/'
                        + notification.requisitionId + '/print')
                ),
                '_blank'
            );

        }
    }
})();