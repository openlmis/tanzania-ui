/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name admin-orderable-edit.controller:OrderableAddEditGeneralController
     *
     * @description
     * Controller for managing orderable view screen.
     */
    angular
        .module('admin-orderable-edit')
        .controller('OrderableAddEditGeneralController', controller);

    controller.$inject = [
        'orderable', '$state', 'OrderableResource', 'FunctionDecorator',
        'successNotificationKey', 'errorNotificationKey',
        'orderableListRelativePath', 'tzOrderableService',
        '$timeout', 'pendingItemList'
    ];

    function controller(orderable, $state, OrderableResource, FunctionDecorator,
                        successNotificationKey, errorNotificationKey,
                        orderableListRelativePath, tzOrderableService,
                        $timeout, pendingItemList) {

        var vm = this,
            isNew;

        vm.$onInit = onInit;
        vm.goToOrderableList  = goToOrderableList;
        vm.saveOrderable = new FunctionDecorator()
            .decorateFunction(saveOrderable)
            .withSuccessNotification(successNotificationKey)
            .withErrorNotification(errorNotificationKey)
            .withLoading(true)
            .getDecoratedFunction();

        vm.acceptOrderable = acceptOrderable;

        vm.pendingItemsList = [];

        /**
         * @ngdoc method
         * @propertyOf admin-orderable-edit.controller:OrderableAddEditGeneralController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating OrderableAddEditGeneralController.
         */
        function onInit() {
            vm.orderable = orderable;
            isNew = !orderable.id;
            vm.pendingItemsList = pendingItemList;
        }

        /**
         * @ngdoc method
         * @methodOf admin-orderable-edit.controller:OrderableAddEditGeneralController
         * @name goToOrderableList
         *
         * @description
         * Redirects to orderable list screen.
         */
        function goToOrderableList() {
            $state.go(orderableListRelativePath, {}, {
                reload: true
            });
        }

        /**
         * @ngdoc method
         * @methodOf admin-orderable-edit.controller:OrderableAddEditGeneralController
         * @name saveOrderable
         *
         * @description
         * Updates the orderable and return to the orderable list on success.
         */
        function saveOrderable() {
            return new OrderableResource()
                .update(vm.orderable)
                .then(function(orderable) {
                    tzOrderableService
                        .deleteMsdOrderable(vm.orderable.productCode)
                        .then(function() {
                        });
                    if (isNew) {
                        $state.go('^.edit.general', {
                            id: orderable.id
                        });
                    } else {
                        goToOrderableList();
                    }
                });
        }

        function acceptOrderable(ord) {
            orderable.productCode = ord.code;
            tzOrderableService.acceptOrderable(orderable)
                .then(function() {
                });

            // Use $timeout to add a delay before calling search
            $timeout(function() {
                tzOrderableService.search(orderable.productCode)
                    .then(function(data) {
                        var dataValue;
                        if (data && data.content && data.content.length > 0) {
                            dataValue = data.content[0];
                            $state.go('^.edit.general', {
                                id: dataValue.id
                            });
                        } else {
                            goToOrderableList();
                        }
                    });
            }, 2000);
        }

    }
})();