/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

  'use strict';

  /**
   * @ngdoc controller
   * @name facility-update-list.controller:FacilityUpdateListController
   *
   * @description
   * Controller for managing facility update list screen.
   */
  angular
  .module('facility-update-list')
  .controller('FacilityUpdateListController', controller);

  controller.$inject = ['$state', '$q', 'pendingFacilityUpdates',
    'facilityUpdateService', 'facilityTypeService',
    'geographicZoneService', 'FacilityRepository', 'loadingModalService',
    'notificationService',
    'confirmService'
  ];

  function controller($state, $q, pendingFacilityUpdates, facilityUpdateService,
      facilityTypeService,
      geographicZoneService, FacilityRepository, loadingModalService,
      notificationService,
      confirmService) {
    var vm = this;

    vm.$onInit = onInit;
    vm.goToAddFacilityPage = goToAddFacilityPage;
    vm.applyFacilityUpdate = applyFacilityUpdate;

    /**
     * @ngdoc property
     * @propertyOf facility-update-list.controller:FacilityUpdateListController
     * @type {Array}
     * @name pendingFacilityUpdates
     *
     * @description
     * The list of all pending facility updates in the system.
     */
    vm.pendingFacilityUpdates = undefined;

    /**
     * @ngdoc method
     * @methodOf facility-update-list.controller:FacilityUpdateListController
     * @name $onInit
     *
     * @description
     * Initialization method of the FacilityUpdateListController.
     */
    function onInit() {
      vm.pendingFacilityUpdates = pendingFacilityUpdates;
    }

    /**
     * @ngdoc method
     * @methodOf facility-update-list.controller:FacilityUpdateListController
     * @name goToAddFacilityPage
     *
     * @description
     * Takes the user to the add facility page.
     */
    function goToAddFacilityPage() {
      $state.go('openlmis.administration.facilities.facility.add');
    }

    /**
     * @ngdoc method
     * @methodOf facility-update-list.controller:FacilityUpdateListController
     * @name applyFacilityUpdate
     *
     * @description
     * Takes the user to the add facility page.
     */

    // eslint-disable-next-line
    async function applyFacilityUpdate(facilityUpdate) {
      await confirmService.confirm('facilityUpdateList.apply.confirm',
          'facilityUpdateList.apply.label');
      loadingModalService.open();

      var ftCode = facilityUpdate.typeCode
          ? facilityUpdate.typeCode
          : 'dhos';
      var ft = await facilityTypeService.query()
      .then(function (response) {
        var ft = response.content.filter(function (type) {
          return type.code === ftCode;
        })[0]

        return ft;
      })

      var gzCode = facilityUpdate.geographicZoneCode
          ? facilityUpdate.geographicZoneCode
          : 'tanz';
      var gz = await geographicZoneService.search({code: gzCode})
      .then(function (response) {
        var gz = response.content[0];
        return gz;
      })

      var facility = {
        active: facilityUpdate.status,
        enabled: facilityUpdate.status,
        code: facilityUpdate.facilityCode,
        name: facilityUpdate.facilityName,
        geographicZone: gz,
        type: ft,
        extraData: {
          hfrCode: facilityUpdate.hfrCode
        }
      };

      try {
        facility = await new FacilityRepository().create(facility);
        facilityUpdate = await facilityUpdateService.markApplied(
            facilityUpdate.id);
        notificationService.success('adminFacilityAdd.facilityHasBeenSaved');
        $state.go('openlmis.administration.facilities.edit', {id: facility.id});
        loadingModalService.close();
      } catch (err) {
        notificationService.error('adminFacilityAdd.failedToSaveFacility');
        loadingModalService.close();
      }

    }

  }

})();