/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name facility-update-list.FacilityUpdateService
     *
     * @description
     * Responsible for retrieving all facility update information from server.
     */
    angular
        .module('facility-update-list')
        .service('facilityUpdateService', service);

    service.$inject = ['$resource', 'openlmisUrlFactory'];

    function service($resource, openlmisUrlFactory) {

        var resource = $resource(openlmisUrlFactory('/api'),
            {}, {
                query: {
                    url: openlmisUrlFactory('/api/msd-facilities'),
                    method: 'GET',
                    isArray: false
                },
                queryPending: {
                    url: openlmisUrlFactory('/api/msd-facilities/pending'),
                    method: 'GET',
                    isArray: false
                },
                markApplied: {
                    url: openlmisUrlFactory('/api/msd-facilities/:id/applied'),
                    method: 'PUT'
                }
            });

        this.query = query;
        this.queryPending = queryPending;
        this.markApplied = markApplied;

        /**
         * @ngdoc method
         * @methodOf facility-update-list.FacilityUpdateService
         * @name query
         *
         * @description
         * Retrieves all facility updates.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} List of facility update
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf facility-update-list.FacilityUpdateService
         * @name queryPending
         *
         * @description
         * Retrieves pending facility updates.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} List of facility updates
         */
        function queryPending(queryParams) {
            return resource.queryPending(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf facility-update-list.FacilityUpdateService
         * @name markApplied
         *
         * @description mark facility update as applied already.
         *
         * @param  {String}  updateId of facility update to mark as applied already.
         * @return {Promise} affected facility update
         */
        function markApplied(updateId) {
            console.log(updateId);
            return resource.markApplied({
                id: updateId
            },{}).$promise;
        }

    }
})();
