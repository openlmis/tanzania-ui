/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular.module('buq-search').config(routes);

    routes.$inject = ['$stateProvider', 'BUQ_RIGHTS'];

    function routes($stateProvider, BUQ_RIGHTS) {

        $stateProvider.state('openlmis.buq.search', {
            showInNavigation: true,
            label: 'buqSearch.search',
            url: '/searchBuq?program&facility',
            accessRights: [
                BUQ_RIGHTS.PREPARE_BUQ,
                BUQ_RIGHTS.CREATE_FORECASTING,
                BUQ_RIGHTS.AUTHORIZE_FORECASTING,
                BUQ_RIGHTS.APPROVE_BUQ,
                BUQ_RIGHTS.MOH_APPROVAL,
                BUQ_RIGHTS.PORALG_APPROVAL
            ],
            views: {
                '@': {
                    templateUrl: 'buq-search/search.html',
                    controller: 'BuqSearchController',
                    controllerAs: 'vm'
                }
            },
            params: {
                sort: ['createdDate,desc']
            },
            resolve: {

                programList: function(programService, $q) {
                    var deferred = $q.defer();

                    programService.getAll().then(function(programs) {
                        var data = programs.filter(function(program) {
                            return program.name === 'BUQ';
                        });
                        deferred.resolve(data);
                    });

                    return deferred.promise;
                },

                facilities: function(requisitionSearchService) {
                    return requisitionSearchService.getFacilities();
                },
                buqs: function(buqService, $stateParams, periodService, facilityService, $q) {

                    var deferred = $q.defer();
                    if ($stateParams.facility === undefined) {
                        return;
                    }

                    buqService.getBuqs($stateParams.facility)
                        .then(function(data) {
                            var dataV = [];
                            if (Array.isArray(data.content) && data.content.length > 0) {
                                dataV = data.content.filter(function(item) {
                                    return item.status !== 'DRAFT';
                                });

                                angular.forEach(dataV, function(v) {
                                    periodService.get(v.processingPeriodId)
                                        .then(function(period) {
                                            v.processingPeriod = period.name;
                                        });

                                    facilityService.get(v.facilityId)
                                        .then(function(fac) {
                                            v.facilityName = fac.name;
                                        });
                                });

                            }

                            deferred.resolve(dataV);
                        });
                    return deferred.promise;
                }
            }
        });

    }

})();