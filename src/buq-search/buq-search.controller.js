/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */
(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name buq-search.controller:BuqSearchViewController
     *
     * @description
     * Controller for requisition view page.
     */
    angular
        .module('buq-search')
        .controller('BuqSearchController', BuqSearchController);

    BuqSearchController.$inject = [
        '$state', '$filter', '$stateParams', 'facilities', 'offlineService', 'localStorageFactory', 'confirmService',
        'REQUISITION_STATUS', 'requisitionService', 'TB_STORAGE', 'LEPROSY_STORAGE',
        'RequisitionViewService', 'programList', 'buqs'
    ];

    function BuqSearchController($state, $filter, $stateParams, facilities, offlineService, localStorageFactory,
                                 confirmService, REQUISITION_STATUS, requisitionService,
                                 TB_STORAGE, LEPROSY_STORAGE, buqProgram, programList, buqs) {

        var vm = this;

        vm.$onInit = onInit;
        vm.search = search;
        vm.openRnr = openRnr;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name facilities
         * @type {Array}
         *
         * @description
         * The list of all facilities available to the user.
         */
        vm.facilities = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name statuses
         * @type {Array}
         *
         * @description
         * Contains all available requisition statuses.
         */
        vm.statuses = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name searchOffline
         * @type {Boolean}
         *
         * @description
         * Flag defining whether online or offline search should done. If it is set to true
         * the local storage will be searched for requisitions.
         */
        vm.searchOffline = false;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name selectedFacility
         * @type {Object}
         *
         * @description
         * The facility selected by the user.
         */
        vm.selectedFacility = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name selectedProgram
         * @type {Object}
         *
         * @description
         * The program selected by the user.
         */
        vm.selectedProgram = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name selectedStatus
         * @type {String}
         *
         * @description
         * The requisition status selected by the user.
         */
        vm.selectedStatus = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name startDate
         * @type {Object}
         *
         * @description
         * The beginning of the period to search for requisitions.
         */
        vm.startDate = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name endDate
         * @type {Object}
         *
         * @description
         * The end of the period to search for requisitions.
         */
        vm.endDate = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name requisitions
         * @type {Array}
         *
         * @description
         * Holds all requisitions that will be displayed on screen.
         */
        vm.requisitions = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-search.controller:BuqSearchViewController
         * @name offline
         * @type {Boolean}
         *
         * @description
         * Indicates if requisitions will be searched offline or online.
         */
        vm.offline = undefined;

        vm.program = undefined;

        vm.options = {
            'requisitionSearch.dateInitiated': ['createdDate,desc']
        };

        /**
         * @ngdoc method
         * @methodOf buq-search.controller:BuqSearchViewController
         * @name $onInit
         *
         * @description
         * Initialization method called after the controller has been created. Responsible for
         * setting data to be available on the view.
         */
        function onInit() {
            vm.program = programList;

            vm.requisitions = buqs;

            vm.facilities = facilities;

            vm.statuses = REQUISITION_STATUS.$toList();

            vm.offline = $stateParams.offline === 'true' || offlineService.isOffline();

            if ($stateParams.facility) {
                vm.selectedFacility = $filter('filter')(vm.facilities, {
                    id: $stateParams.facility
                })[0];
            }

            if (vm.selectedFacility && $stateParams.program) {
                vm.selectedProgram = $filter('filter')(vm.selectedFacility.supportedPrograms, {
                    id: $stateParams.program
                })[0];
            }

            if ($stateParams.initiatedDateFrom) {
                vm.startDate = $stateParams.initiatedDateFrom;
            }

            if ($stateParams.initiatedDateTo) {
                vm.endDate = $stateParams.initiatedDateTo;
            }

            if ($stateParams.requisitionStatus) {
                vm.selectedStatus = $stateParams.requisitionStatus;
            }
        }

        /**
         * @ngdoc method
         * @methodOf buq-search.controller:BuqSearchViewController
         * @name openRnr
         *
         * @description
         * Redirect to requisition page after clicking on grid row.
         *
         * @param {String} requisitionId Requisition UUID
         */
        function openRnr(requisition) {

            if (typeof requisition === 'object') {
                redirectForecasting(requisition);
            }
        }

        /**
         * @ngdoc method
         * @methodOf buq-search.controller:BuqSearchViewController
         * @name search
         *
         * @description
         * Searches requisitions by criteria selected in form.
         */
        function search() {
            var stateParams = angular.copy($stateParams);
            stateParams.program = vm.program.length > 0 ? vm.program[0].id : null;
            stateParams.facility = vm.selectedFacility ? vm.selectedFacility.id : null;
            stateParams.initiatedDateFrom = vm.startDate ? $filter('isoDate')(vm.startDate) : null;
            stateParams.initiatedDateTo = vm.endDate ? $filter('isoDate')(vm.endDate) : null;
            stateParams.offline = vm.offline;
            stateParams.requisitionStatus = vm.selectedStatus;

            if (stateParams.facility === null) {
                return;
            }

            $state.go('openlmis.buq.search', stateParams, {
                reload: true
            });
        }

        function redirectForecasting(requisition) {
            if (requisition.id) {
                $state.go('openlmis.buq.facilityDemandingForecasting', {
                    id: requisition.id
                });
            }
        }
    }
})();