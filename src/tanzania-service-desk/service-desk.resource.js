/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name tanzania-service-desk.ServiceDeskResource
     *
     * @description
     * Communicates with the Service Desk API of the tanzania OpenLMIS server.
     */
    angular
        .module('tanzania-service-desk')
        .factory('ServiceDeskResource', ServiceDeskResource);

    ServiceDeskResource.inject = ['$resource', 'openlmisUrlFactory', '$http'];

    function ServiceDeskResource($resource, openlmisUrlFactory, $http) {

        ServiceDeskResource.prototype.create = create;
        ServiceDeskResource.prototype.addAttachment = addAttachment;
        $http.defaults.useXDomain = true;
        var resource = $resource(openlmisUrlFactory('/api/support-desk/issues'), {}, {
            addAttachment: {
                url: openlmisUrlFactory('/api/support-desk/issues/:issueId/attachment'),
                method: 'POST',
                headers: {
                    'Content-Type': undefined,
                    'Access-Control-Allow-Origin': '*'
                }
            }
        });

        return ServiceDeskResource;

        function ServiceDeskResource() {
        }

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk.ServiceDeskResource
         * @name create
         * 
         * @description
         * Creates issue for Service Desk API.
         * 
         * @param  {Object}  issue issue to be created
         * @return {Promise}       the promise resolving to the server response, rejected if request fails
         */
        function create(issue) {
            return resource.save(issue).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk.ServiceDeskResource
         * @name addAttachment
         * 
         * @description
         * Attaches file to the Service Desk issue.
         * 
         * @param  {File}    attachment file to be attached
         * @param  {String}  issueId    id of already created issue
         * @return {Promise}            the promise resolving to the server response, rejected if request fails
         */
        function addAttachment(attachment, issueId) {
            var formData = new FormData();
            formData.append('file', attachment);

            return resource.addAttachment({
                issueId: issueId
            }, formData).$promise;
        }

    }
})();