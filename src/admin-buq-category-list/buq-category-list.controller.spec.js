/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

describe('BuqCategoryListController', function() {

    var $state, $controller, vm, buqCategories, BuqCategoryDataBuilder;

    beforeEach(function() {
        module('admin-buq-category');
        module('admin-buq-category-list');

        inject(function($injector) {
            $controller = $injector.get('$controller');
            $state = $injector.get('$state');
            $controller = $injector.get('$controller');
            BuqCategoryDataBuilder = $injector.get('BuqCategoryDataBuilder');
        });

        buqCategories = [
            new BuqCategoryDataBuilder().build(),
            new BuqCategoryDataBuilder().build(),
            new BuqCategoryDataBuilder().build()
        ];

        vm = $controller('BuqCategoryListController', {
            buqCategories: buqCategories
        });

        spyOn($state, 'go').andReturn();
    });

    describe('goToAddBuqCategoryPage', function() {

        it('should redirect user to add add buq category page', function() {
            vm.goToAddBuqCategoryPage();

            expect($state.go).toHaveBeenCalledWith('openlmis.administration.buqCategories.add');
        });
    });
});
