/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name admin-rejection-.controller:BuqCategoryListController
     *
     * @description
     * Controller for listing Buq Categories.
     */
    angular
        .module('admin-buq-category-list')
        .controller('BuqCategoryListController', controller);

    controller.$inject = ['$state', 'buqCategories'];

    function controller($state, buqCategories) {
        var vm = this;

        vm.goToAddBuqCategoryPage = goToAddBuqCategoryPage;

        /**
         * @ngdoc property
         * @propertyOf admin-rejection-.controller:BuqCategoryListController
         * @name buqCategories
         * @type {Object}
         *
         * @description
         * Buq Category object
         */
        vm.buqCategories = buqCategories;

        /**
         * @ngdoc property
         * @propertyOf admin-rejection-.controller:BuqCategoryListController
         * @name invalidMessage
         * @type {String}
         *
         * @description
         * Holds form error message.
         */
        vm.invalidMessage = undefined;

        /**
         * @ngdoc method
         * @methodOf admin-rejection-.controller:BuqCategoryListController
         * @name goToAddBuqCategoryPage
         *
         * @description
         * Take you to a page for adding reason category.
         */
        function goToAddBuqCategoryPage() {
            $state.go('openlmis.administration.buqCategories.add');
        }
    }

})();
