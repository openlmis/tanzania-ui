import React, { useState } from 'react';

function SearchComponent({ onSearch, title, subtitle }) {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    onSearch(event.target.value);
  };

  const handleSearchSubmit = (event) => {
    event.preventDefault();
    onSearch(searchTerm);
  };

  return (
      <form onSubmit={handleSearchSubmit} className={'search-form1'}>
        <h6>{title}</h6>
        <input
            type="text"
            placeholder="Search..."
            value={searchTerm}
            onChange={handleSearchChange}
        />
        <p>{subtitle}</p>
      </form>
  );
}

export default SearchComponent;
