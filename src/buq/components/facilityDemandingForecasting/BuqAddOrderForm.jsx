import React, { useEffect, useMemo, useState, useRef } from "react";
import ModalErrorMessage from "../../../react-components/ModalErrorMessage";
import Checkbox from '../../../react-components/inputs/checkbox';
import TableNoPagination from "../../../react-components/table/table-no-pagination";
import ResponsiveButton from "../../../react-components/buttons/responsive-button";
import SearchComponent from "./SearchComponent";

  export default function BuqAddOrderForm({orderables, onCancel, onHandleAddProduct}) {
  const [displayValidationError, setDisplayValidationError] = useState(false);
  const [selectedOrders, setSelectedOrders] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [orderableOptions, setOrderableOptions] = useState([]);
  const handleSearch = (searchTerm) => {
          setSearchTerm(searchTerm);
  };
  useEffect(() => {
      const filteredOrderables = orderables?.filter(item =>
          !searchTerm ||
          item?.productCode?.toLowerCase().includes(searchTerm.toLowerCase()) ||
          item?.fullProductName?.toLowerCase().includes(searchTerm.toLowerCase())
      );
          //const filteredOrderables = orderables?.filter(item=>!searchTerm || item?.productCode?.includes(searchTerm) || item?.fullProductName?.includes(searchTerm));
      setOrderableOptions(filteredOrderables);
      }, [orderables, searchTerm]);

  const toggleRowCheckbox = (value, row) => {

          setSelectedOrders(prevSelectedOrders => {
              if (value) {
                  return [...prevSelectedOrders, row];
              } else {
                  return prevSelectedOrders.filter(item => item.id !== row.id);
              }
          });
      };
  
  const  handleSave = () =>{
      onHandleAddProduct(selectedOrders);
  }

  const columns = useMemo(
    () => [

      {
        Header: 'Select',
        accessor: 'checkbox',
        Cell: ({ row }) => (
          <div className="prepare-buq-table-actions">
            <Checkbox
              name={row.original.id}
              checked={selectedOrders?.some(item=> item.id == row.original.id)}
              onClick={(value) => toggleRowCheckbox(value, row.original)}
            />
          </div>
        ),
      },
      {
        Header: 'Code',
        accessor: 'productCode',
      }, 
      {
        Header: 'Product',
        accessor: 'fullProductName',
      },
      {
        Header: 'Unit Of Issue',
        accessor: 'dispensable.displayUnit',
      },
      
    ],
    []
  );

  return (
    <>
        <div className="page-container">
            <div className="page-header-responsive header-sof-modal">
                <h2>Add Products </h2>
            </div>
            <div><SearchComponent title={'Search Product'} subtitle={'Please type in a part of the product name or the first few letters of the product code'} onSearch={handleSearch}/></div>
            <div className="table-container">
                <TableNoPagination
                    customReactTableContainerStyle="source-of-fund-table-container"
                    customReactTableContentStyle="custom-react-table-content"
                    customReactTableStyle="custom-react-table"
                    columns={columns}
                    data={orderableOptions}
                />
            </div>

            <div className="bottom-bar">
                <div>
                    <button type="button" className="secondary" onClick={onCancel}>
                        Cancel
                    </button>
                </div>
                <div>

                    <ResponsiveButton
                        onClick={() => handleSave()}
                        className="btn btn-primary"
                    >
                        Add Products
                    </ResponsiveButton>
                </div>
            </div>
        </div>
        <ModalErrorMessage
            isOpen={displayValidationError}
            onClose={() => setDisplayValidationError(false)}
      />
    </>
  );
}
