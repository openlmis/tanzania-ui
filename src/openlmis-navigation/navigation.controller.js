/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name openlmis-navigation.controller:NavigationController
     *
     * @description
     * Adds functionality that takes a state's label property and uses the messageService to translate it into string.
     */

    angular
        .module('openlmis-navigation')
        .controller('NavigationController', NavigationController);

    NavigationController.$inject = ['$scope', 'navigationStateService'];

    function NavigationController($scope, navigationStateService) {

        var vm = this;

        vm.$onInit = onInit;
        vm.hasChildren = navigationStateService.hasChildren;
        vm.isSubmenu = navigationStateService.isSubmenu;
        vm.isOffline = navigationStateService.isOffline;

        /**
         * @ngdoc property
         * @propertyOf openlmis-navigation.controller:NavigationController
         * @name states
         * @type {Array}
         *
         * @description
         * Contains all states in application in tree structure.
         */
        vm.states = undefined;

        /**
         * @ngdoc method
         * @methodOf openlmis-navigation.controller:NavigationController
         * @name onInit
         *
         * @description
         * Initialization method for NavigationController.
         */
        function onInit() {
            setStates();

            $scope.groupedData = [];

            var originalData = _.where(vm.states, {
                name: 'openlmis.administration'
            });

            if (originalData && originalData.length > 0 && originalData[0].children) {
                $scope.groupedData = groupData(originalData[0].children, groups);
            } else {
                $scope.groupedData = [];
            }

            if ($scope.groupedData.length > 0) {

                vm.adminMenuList = [{
                    $shouldDisplay: originalData[0].$shouldDisplay,
                    abstract: originalData[0].abstract,
                    label: originalData[0].label,
                    name: originalData[0].name,
                    priority: originalData[0].priority,
                    showInNavigation: originalData[0].showInNavigation,
                    template: originalData[0].template,
                    url: originalData[0].url,
                    children: $scope.groupedData
                }];

            }

        }

        function setStates() {
            if (!$scope.rootState && !$scope.states) {
                vm.states = navigationStateService.roots[''];
            } else if ($scope.rootState) {
                vm.states = navigationStateService.roots[$scope.rootState];
            } else {
                vm.states = $scope.states;
            }
        }

        // Groups to match against
        var groups = [
            {
                label: 'Equipment',
                name: 'Equipment',
                key: 'equipment',
                group: ['openlmis.equipment', 'adminCceUpload.equipment',
                    'openlmis.administration.cce',
                    'openlmis.administration.discipline',
                    'openlmis.administration.equipment',
                    'openlmis.administration.equipmentCategory',
                    'openlmis.administration.equipmentContracts',
                    'openlmis.administration.equipmentEnergyTypes',
                    'openlmis.administration.equipmentFundSources',
                    'openlmis.administration.equipmentModels',
                    'openlmis.administration.equipments',
                    'openlmis.administration.equipmentServiceTypes',
                    'openlmis.administration.equipmentsOrderable',
                    'openlmis.administration.equipmentTestType',
                    'openlmis.administration.equipmentTypes',
                    'openlmis.administration.equipmentVendors',
                    'openlmis.administration.programEquipments',
                    'openlmis.administration.reasonsNotFunctional',
                    'openlmis.administration.operationModes',
                    'openlmis.administration.lots']
            },
            {
                label: 'Manage',
                name: 'Manage',
                key: 'manage',
                group: ['openlmis.administration.facilities',
                    'openlmis.administration.facilityTypes',
                    'openlmis.administration.geographicZones',
                    'openlmis.administration.isa',
                    'openlmis.administration.orderables',
                    'openlmis.administration.processingSchedules',
                    'openlmis.administration.programs',
                    'openlmis.administration.reasons',
                    'openlmis.administration.reasonsNotFunctional',
                    'openlmis.administration.rejectionReasonCategories',
                    'openlmis.administration.roles',
                    'openlmis.administration.requisitionGroupList',
                    'openlmis.administration.requisitionTemplates',
                    'openlmis.administration.supervisoryNodes',
                    'openlmis.administration.supplyLines',
                    'openlmis.administration.users',
                    'openlmis.administration.rejectionReasons',
                    'openlmis.administration.buqCategories']
            }
        ];

        // Function to group the data
        function groupData(data, groups) {
            var groupedData = [];

            groups.forEach(function(group) {
                var groupItems = {
                    name: group.name,
                    key: group.key,
                    children: []
                };

                group.group.forEach(function(groupName) {
                    var matchingItem = data.find(function(item) {
                        return item.name === groupName;
                    });

                    if (matchingItem) {
                        groupItems.children.push(matchingItem);
                    }
                });

                groupedData.push(groupItems);
            });

            // Group items with no group into "Other" group
            var otherGroup = {
                name: 'Other',
                key: 'other',
                children: data.filter(function(item) {
                    // Filter items with no group
                    return !groups.some(function(group) {
                        return group.group.includes(item.name);
                    });
                })
            };

            if (otherGroup.children.length > 0) {
                groupedData.push(otherGroup);
            }

            return groupedData;
        }

        // Function to handle submenu click
        $scope.toggleSubmenu = function(event) {
            // Find the next 'ul' element and toggle its visibility
            angular.element(event.currentTarget).next('ul')
                .toggle();

            // Prevent event propagation and default action
            event.stopPropagation();
            event.preventDefault();
        };

    }
})();