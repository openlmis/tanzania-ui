/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name tanzania-service-desk-list.serviceDeskListService
     *
     * @description
     * Responsible for retrieving all information from server.
     */
    angular
        .module('tanzania-service-desk-list')
        .service('serviceDeskListService', service);

    service.$inject = ['$resource', 'openlmisUrlFactory', 'commonUrlFactory'];

    function service($resource, openlmisUrlFactory, commonUrlFactory) {

        var resource = $resource(commonUrlFactory('/api/support-desk/:id'), {}, {

            query: {
                url: commonUrlFactory('/api/support-desk/getArticlesBySearchKeyword'),
                method: 'GET',
                isArray: false
            },
            get: {
                url: commonUrlFactory('/api/support-desk/reportIssueOnSd'),
                method: 'GET',
                isArray: false
            },

            addAttachment: {
                url: commonUrlFactory('/api/support-desk/issues/:issueId/attachment'),
                method: 'POST',
                headers: {
                    'Content-Type': undefined,
                    'Access-Control-Allow-Origin': '*'
                }
            }
        });

        this.addAttachment = addAttachment;
        this.query = query;
        this.get = get;

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk.ServiceDeskResource
         * @name addAttachment
         *
         * @description
         * Attaches file to the Service Desk issue.
         *
         * @param  {File}    attachment file to be attached
         * @param  {String}  issueId    id of already created issue
         * @return {Promise}            the promise resolving to the server response, rejected if request fails
         */
        function addAttachment(attachment, issueId) {
            var formData = new FormData();
            formData.append('file', attachment);

            return resource.addAttachment({
                issueId: issueId
            }, formData).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk.ServiceDeskResource
         * @name query
         *
         * @description
         * Retrieves all articles by type.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Page of articles
         */
        function query(queryParams) {
            return resource.query(queryParams).$promise;
        }

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk.ServiceDeskResource
         * @name get
         *
         * @description
         * Retrieves all articles by type.
         *
         * @param  {Object}  queryParams the search parameters
         * @return {Promise} Page of articles
         */
        function get(queryParams) {
            return resource.get(queryParams).$promise;
        }

    }
})();
