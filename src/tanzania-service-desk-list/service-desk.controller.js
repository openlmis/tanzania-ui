/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name tanzania-service-desk-list:ServiceDeskListController
     *
     * @description
     * Controller for managing supply line list screen.
     */
    angular
        .module('tanzania-service-desk-list')
        .controller('ServiceDeskListController', controller);

    controller.$inject = [
        '$q', '$state', '$location', 'issueTypes', 'priorities', 'impactTypes', 'serviceDeskListService',
        'notificationService', 'user', 'messageService', 'loadingModalService', 'alertService',
        'archives', 'faqArticles', '$stateParams', '$sce'
    ];

    function controller($q, $state, $location, issueTypes, priorities, impactTypes, serviceDeskListService,
                        notificationService, user, messageService, loadingModalService, alertService,
                        archives, faqArticles, $stateParams, $sce) {
        var vm = this;

        vm.$onInit = onInit;
        vm.send = send;
        vm.redirectToHome = redirectToHome;
        vm.viewArchiveContent = viewArchiveContent;
        vm.viewArticleContent = viewArticleContent;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name showIframeData
         * @type boolean
         *
         * @description
         * Flag to alternate view of iframe data.
         */
        vm.showArticleData = false;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name showIframeData
         * @type boolean
         *
         * @description
         * Flag to alternate view of iframe data.
         */
        vm.showIframeData = false;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name showIframeData
         * @type boolean
         *
         * @description
         * Flag to alternate view of iframe data.
         */
        vm.isReferenceMaterials = false;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name iframeSrc
         * @type {Object}
         *
         * @description
         * Contains all available types of archives.
         */
        vm.iframeSrc = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name title
         * @type {Object}
         *
         * @description
         * Contains all available types of archives.
         */
        vm.title = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name archives
         * @type {Object}
         *
         * @description
         * Contains all available types of archives.
         */
        vm.archives = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name faqArticles
         * @type {Object}
         *
         * @description
         * Contains all available types of faqArticles.
         */
        vm.faqArticles = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name issueTypes
         * @type {Array}
         *
         * @description
         * Contains all available types of issue.
         */
        vm.issueTypes = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name priorities
         * @type {Array}
         *
         * @description
         * Contains all available priorities.
         */
        vm.priorities = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name impacts
         * @type {Array}
         *
         * @description
         * Contains all available types of impacts.
         */
        vm.impactTypes = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name issue
         * @type {Object}
         *
         * @description
         * Contains issue to be send to service desk API.
         */
        vm.issue = undefined;

        /**
         * @ngdoc property
         * @propertyOf tanzania-service-desk-list:ServiceDeskListController
         * @name attachments
         * @type {Array}
         *
         * @description
         * Contains all files that will be attached to Service Desk issue.
         */
        vm.attachments = undefined;

        /**
         * @ngdoc property
         * @methodOf tanzania-service-desk-list:ServiceDeskListController
         * @name selectedTab
         * @type {String}
         *
         * @description
         * Contains currently selected tab.
         */
        vm.selectedTab = undefined;

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk-list:ServiceDeskListController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating ServiceDeskListController.
         */
        function onInit() {
            vm.issueTypes = issueTypes;
            vm.priorities = priorities;
            vm.impactTypes = impactTypes;
            vm.issue = {};
            vm.attachments = [];
            vm.archives = archives;
            vm.faqArticles = faqArticles;

            if ($stateParams.selectedTab === undefined) {
                vm.selectedTab = 0;
            }

            if (parseInt($stateParams.selectedTab, 10) === 1) {
                vm.selectedTab = 1;
                vm.title = undefined;
                vm.title = $stateParams.title;
                vm.showIframeData = true;
                vm.iframeSrc = undefined;
                vm.iframeSrc = $sce.trustAsResourceUrl($stateParams.iframeSrc);
            }

            if (parseInt($stateParams.selectedTab, 10) === 0) {
                vm.selectedTab = 0;
                vm.title = undefined;
                vm.title = $stateParams.title;
                vm.showIframeData = true;
                vm.iframeSrc = $sce.trustAsResourceUrl($stateParams.iframeSrc);
            }

            if (parseInt($stateParams.selectedTab, 10) === 2) {
                vm.selectedTab = 2;
            }

        }

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk-list:ServiceDeskListController
         * @name send
         *
         * @description
         * Reloads page with new search parameters.
         */
        function send() {

            loadingModalService.open();
            return serviceDeskListService.get(vm.issue)
                .then(function(response) {
                    var attachmentPromises = [];
                    vm.attachments.forEach(function(attachment) {
                        attachmentPromises.push(serviceDeskListService.addAttachment(attachment, response.issueId));
                    });

                    return $q.all(attachmentPromises)
                        .then(function() {
                            var successMessage = messageService.get('serviceDesk.sendSuccessfully', {
                                ticketNumber: response.issueKey,
                                userEmailAddress: user.email
                            });
                            notificationService.success(successMessage);
                            redirectToHome();
                        })
                        .catch(function() {
                            alertService.error('serviceDesk.attachmentTooLarge');
                            loadingModalService.close();
                        });
                })
                .catch(loadingModalService.close);
        }

        function viewArchiveContent(article) {
            vm.iframeSrc = undefined;
            vm.title = undefined;
            var stateParams = angular.copy($stateParams);
            stateParams.title = article.title;
            stateParams.iframeSrc = $sce.trustAsResourceUrl(article.content.iframeSrc);
            stateParams.isReferenceMaterials = true;
            stateParams.selectedTab = 1;

            $state.go('openlmis.serviceDeskList', stateParams, {
                reload: true
            });
        }

        function viewArticleContent(article2) {
            vm.iframeSrc = undefined;
            vm.title = undefined;
            var stateParams = angular.copy($stateParams);
            stateParams.title = article2.title;
            stateParams.iframeSrc = $sce.trustAsResourceUrl(article2.content.iframeSrc);
            stateParams.isReferenceMaterials = false;
            stateParams.selectedTab = 0;

            $state.go('openlmis.serviceDeskList', stateParams, {
                reload: true
            });
        }

        /**
         * @ngdoc method
         * @methodOf tanzania-service-desk-list:ServiceDeskListController
         * @name redirectToHome
         *
         * @description
         * Redirects user to home page.
         */
        function redirectToHome() {
            $state.go('openlmis.home');
        }

    }
})();
