/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name requisition-malaria-validation-view-tab.controller:MalariaValidationViewTabController
     *
     * @description
     * Responsible for managing patients grid.
     */
    angular
        .module('requisition-view-tab')
        .controller('MalariaValidationViewTabController', MalariaValidationViewTabController);

    MalariaValidationViewTabController.$inject = ['localStorageFactory', 'requisition', 'requisitionService'];
    function MalariaValidationViewTabController(localStorageFactory, requisition, requisitionService) {
        var vm = this;

        vm.$onInit = onInit;

        vm.columns = undefined;

        function loadColumns() {
            vm.columns = [
                {
                    name: 'productCode',
                    label: 'requisitionMalariaValidationViewTab.productCode.label',
                    left: true
                },
                {
                    name: 'fullProductName',
                    label: 'requisitionMalariaValidationViewTab.fullProductName.label',
                    left: true
                },
                {
                    name: 'dispensingUnit',
                    label: 'requisitionMalariaValidationViewTab.dispensingUnit.label',
                    left: true
                },
                {
                    name: 'totalPatient',
                    label: 'requisitionMalariaValidationViewTab.totalPatient.label',
                    left: false
                },
                {
                    name: 'totalConsumption',
                    label: 'requisitionMalariaValidationViewTab.totalConsumption.label',
                    left: false
                }
            ];
        }

        vm.formatNumber = function(number) {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        };

        function loadMalariaData() {

            requisitionService.getMalariaData(
                requisition.id,
                requisition.facility.id
            )
                .then(function(malaria) {
                    vm.malariaLineItems = malaria;
                });
        }

        vm.malariaLineItems = [];
        function onInit() {
            loadColumns();
            loadMalariaData();
        }

    }

})();
