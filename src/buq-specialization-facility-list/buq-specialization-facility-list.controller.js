/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
     *
     * @description
     * Controller for managing specFacilities list screen.
     */
    angular
        .module('buq-specialization-facility-list')
        .controller('BuqSpecializationFacilityListController', controller);

    controller.$inject = ['buqSpecialization', 'buqSpecFacilities', 'facilities',
        'facilitiesMap', 'stateTrackerService', 'buqSpecializationService'];

    function controller(buqSpecialization, buqSpecFacilities, facilities,
                        facilitiesMap, stateTrackerService, buqSpecializationService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.addFacility = addFacility;
        vm.removeFacility = removeFacility;
        vm.saveBuqSpecialization = saveBuqSpecialization;
        vm.goToBuqSpecializationList = goToBuqSpecializationList;
        vm.toFacilityIdName = toFacilityIdName;

        /**
         * @ngdoc property
         * @propertyOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @type {Array}
         * @name buqSpecFacilities
         *
         * @description
         * The list of all available specFacilities in the system.
         */
        vm.buqSpecFacilities = undefined;

        /**
         * @ngdoc property
         * @propertyOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @type {BuqSpecialization}
         * @name buqSpecialization
         *
         * @description
         * BuqSpecialization that is being created.
         */
        vm.buqSpecialization = undefined;

        /**
         * @ngdoc method
         * @methodOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @name $onInit
         *
         * @description
         * Initialization method of the BuqSpecializationFacilityListController.
         */
        function onInit() {
            vm.buqSpecFacilities = buqSpecFacilities;
            vm.buqSpecialization = buqSpecialization;
            vm.facilities = facilities;
            vm.facilitiesMap = facilitiesMap;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @name addFacilities
         *
         * @description
         * Method that displays a modal for selecting and adding a facility to the UI
         */
        function addFacility() {
            if (vm.buqSpecFacilities.indexOf(vm.selectedFacility) === -1) {
                vm.buqSpecFacilities.push(vm.selectedFacility);
                vm.buqSpecialization.facilities = vm.buqSpecFacilities;
            }

            vm.selectedFacility = undefined;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @name removeFacility
         *
         * @description
         * Method that removes kit constituent from the specialization facilities
         *
         * @param {Object} a single facility constituent to be removed
         */
        function removeFacility(facility) {
            if (vm.buqSpecFacilities.indexOf(facility) > -1) {
                vm.buqSpecFacilities.splice(vm.buqSpecFacilities.indexOf(facility), 1);
            }

            vm.buqSpecialization.facilities = vm.buqSpecFacilities;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @name goToBuqSpecializationList
         *
         * @description
         * Redirects to buqSpecialization list screen.
         */
        function goToBuqSpecializationList() {
            stateTrackerService.goToPreviousState(
                'openlmis.administration.buqSpecializations', {},
                {
                    reload: true
                }
            );
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @name saveBuqSpecialization
         *
         * @description
         * Updates the buqSpecialization and return to the buqSpecialization list on success.
         */
        function saveBuqSpecialization() {
            vm.buqSpecialization.facilityIds = [];
            vm.buqSpecialization.facilities.forEach(function(facility) {
                vm.buqSpecialization.facilityIds.push(facility.id);
            });
            return buqSpecializationService.update(vm.buqSpecialization)
                .then(goToBuqSpecializationList);
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-facility-list.controller:BuqSpecializationFacilityListController
         * @name toFacilityIdName
         *
         * @description
         * Gets facility's name using its id.
         *
         * @return {String} the name of the facility.
         */
        function toFacilityIdName(facilityId) {
            var facility = vm.facilitiesMap[facilityId];
            return facility.code + ' : ' + facility.name;
        }
    }

})();