/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular.module('buq-specialization-facility-list').config(routes);

    routes.$inject =
        ['$stateProvider', 'selectProductsModalStateProvider', 'ADMINISTRATION_RIGHTS'];

    function routes($stateProvider, ADMINISTRATION_RIGHTS) {
        $stateProvider.state('openlmis.administration.buqSpecializationFacilities', {
            showInNavigation: false,
            label: 'buqSpecializationFacilityList.facilities',
            url: '/:id/facilities?page&size',
            controller: 'BuqSpecializationFacilityListController',
            controllerAs: 'vm',
            nonTrackable: true,
            templateUrl: 'buq-specialization-facility-list/buq-specialization-facility-list.html',
            accessRights: [ADMINISTRATION_RIGHTS.MANAGE_BUQ],
            resolve: {
                buqSpecialization: function(
                    BuqSpecialization, buqSpecializationService, $stateParams
                ) {
                    return buqSpecializationService.get($stateParams.id)
                        .then(function(specialization) {
                            return new BuqSpecialization(specialization);
                        });
                },
                buqSpecFacilities: function(
                    buqSpecialization, paginationService, $stateParams
                ) {
                    return paginationService.registerList(null, $stateParams, function() {
                        return buqSpecialization.facilities;
                    }, {
                        paginationId: 'buqSpecFacilityList'
                    });
                },
                facilities: function(FacilityResource) {
                    return new FacilityResource()
                        .query()
                        .then(function(facilities) {
                            return facilities.content;
                        });
                },
                // facilities: function(facilityService) {
                //     return facilityService.getAllMinimal();
                // },
                facilitiesMap: function(facilities, ObjectMapper) {
                    return new ObjectMapper().map(facilities);
                }
            }
        });
    }
})();
