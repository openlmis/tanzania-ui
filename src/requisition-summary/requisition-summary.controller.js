/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name requisition-summary.controller:RequisitionSummaryController
     *
     * @description
     * Responsible for managing Requisition Total Cost and other sources of fund popover
     */
    angular
        .module('requisition-summary')
        .controller('RequisitionSummaryController', controller);

    controller.$inject = ['$scope', '$filter', 'calculationFactory', 'sourceOfFundService', '$timeout',
        'messageService', 'MAX_INTEGER_VALUE', 'requisitionBudgetService', '$rootScope'];

    function controller($scope, $filter, calculationFactory, sourceOfFundService, $timeout,
                        messageService, MAX_INTEGER_VALUE, requisitionBudgetService, $rootScope) {
        var vm = this;

        vm.calculateFullSupplyCost = calculateFullSupplyCost;
        vm.calculateNonFullSupplyCost = calculateNonFullSupplyCost;
        vm.calculateTotalCost = calculateTotalCost;
        vm.calculateTotalSupplementalFunds = calculateTotalSupplementalFunds;
        vm.validateRequisitionCost = validateRequisitionCost;

        /**
         * @ngdoc property
         * @propertyOf requisition-summary.controller:RequisitionSummaryController
         * @name budgetRequisitions
         * @type {Object}
         *
         * @description
         * The budget requisitions to render the summary for.
         */
        vm.otherSourcesOfFunds = [];

        /**
         * @ngdoc property
         * @propertyOf requisition-summary.controller:RequisitionSummaryController
         * @name requisition
         * @type {Object}
         *
         * @description
         * The requisition to render the summary for.
         */
        vm.requisition = $scope.requisition;

        /**
         * @ngdoc property
         * @propertyOf requisition-summary.controller:RequisitionSummaryController
         * @name showNonFullSupplySummary
         * @type {Boolean}
         *
         * @description
         * Flag dictating whether non full supply summary should be visible inside the popover.
         */
        vm.showNonFullSupplySummary = vm.requisition.program.showNonFullSupplyTab;

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name calculateFullSupplyCost
         *
         * @description
         * Calculates total cost of all full supply line items. This method will ignore skipped
         * line items.
         *
         * @return {Number} the total cost of all full supply line items
         */
        function calculateFullSupplyCost() {
            return calculateCost(true);
        }

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name calculateNonFullSupplyCost
         *
         * @description
         * Calculates total cost of all non full supply line items. This method will ignore skipped
         * line items.
         *
         * @return {Number} the total cost of all non full supply line items
         */
        function calculateNonFullSupplyCost() {
            return calculateCost(false);
        }

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name calculateTotalCost
         *
         * @description
         * Calculates total cost of all line items. This method will ignore skipped line items.
         *
         * @return {Number} the total cost of all line items
         */
        function calculateTotalCost() {
            return calculateCost();
        }

        function calculateCost(fullSupply) {
            var sum = 0;

            getLineItems(fullSupply).forEach(function(lineItem) {
                if (!lineItem.skipped) {
                    sum += calculationFactory.totalCost(lineItem, vm.requisition);
                }
            });

            return sum;
        }

        function getLineItems(fullSupply) {
            var lineItems;

            if (fullSupply === undefined) {
                lineItems = vm.requisition.requisitionLineItems;
            } else {
                lineItems = $filter('filter')(vm.requisition.requisitionLineItems, {
                    $program: {
                        fullSupply: fullSupply
                    }
                });
            }

            return lineItems;
        }

        vm.$onInit = onInit;

        /**
         * @ngdoc property
         * @propertyOf requisition-summary.controller:RequisitionSummaryController
         * @name addedItems
         * @type {Array}
         *
         * @description
         * Source of funds that users have chosen in this popover.
         */
        vm.addedItems = [];

        /**
         * @ngdoc property
         * @propertyOf requisition-summary.controller:RequisitionSummaryController
         * @type {Array}
         * @name sourceOfFunds
         *
         * @description
         * The list of all available sources of funds in the system.
         */
        vm.sourceOfFunds = undefined;

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name addOneSourceOfFund
         *
         * @description
         * Add the currently selected source of fund into the table beneath it for users to do further actions.
         */
        vm.addOneSourceOfFund = function() {

            var notAlreadyAdded = !_.contains(vm.addedItems, vm.selectedSourceOfFund);

            if (notAlreadyAdded) {
                vm.addedItems.push(vm.selectedSourceOfFund);
            }
        };

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name sourceOfFundSelectionChanged
         *
         * @description
         * Reset form status and change content inside source of fund drop down list.
         */
        vm.sourceOfFundSelectionChanged = function() {
            //reset selected source of fund, so that source of fund field has no default value
            vm.selectedSourceOfFund = null;

            //same as above
            $scope.sourceOfFundForm.$setUntouched();

            //make form good as new, so errors won't persist
            $scope.sourceOfFundForm.$setPristine();

        };

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name removeAddedSourceOfFund
         *
         * @description
         * Removes an already added Source Of Fund and reset its quantity value.
         */
        vm.removeAddedSourceOfFund = function(item) {
            item.id = undefined;
            item.budgetAmountMissingError = undefined;
            vm.addedItems = _.without(vm.addedItems, item);
        };

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name validate
         *
         * @description
         * Validate if budgetAmount is filled in by user.
         */
        vm.validate = function(item) {
            if (item.budgetAmount === 0 || item.budgetAmount === null) {
                item.quantityInvalid = messageService.get('sourceOfFund.required');
            } else if (item.budgetAmount > MAX_INTEGER_VALUE) {
                item.quantityInvalid = messageService.get('sourceOfFund.numberTooLarge');
            } else {
                item.quantityInvalid = undefined;
            }
        };

        /**
         * @ngdoc property
         * @propertyOf requisition-summary.controller:RequisitionSummaryController
         * @type {Array}
         * @name budgetAmount
         *
         * @description
         * The shows the allocated budget amount provided to specific facility.
         */
        vm.budgetAmount = undefined;

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name $onInit
         *
         * @description
         * Initialization method of the SourcesOfFundListController.
         */
        function onInit() {

            $timeout(function() {
                requisitionBudgetService.getByFacilityId(vm.requisition.facility.id)
                    .then(function(response) {
                        vm.budgetAmount = getOnlyAllocatedBudget(response.data);
                    });

            }, 100);
        }

        function getOnlyAllocatedBudget(facilityBudgets) {

            var filterAllocatedBudgets = [];

            if (facilityBudgets.length > 0) {

                filterAllocatedBudgets = $filter('filter')(facilityBudgets, function(budget) {
                    return budget.sourceOfFundName.toLowerCase() === 'msd';
                });

            }
            return filterAllocatedBudgets[0].budgetAmount;
        }

        $rootScope.$on('otherSourcesOfFund', function(event, otherSourcesOfFunds) {
            vm.otherSourcesOfFunds = otherSourcesOfFunds;
        });

        /**
         * @ngdoc method
         * @methodOf requisition-summary.controller:RequisitionSummaryController
         * @name calculateTotalSupplementalFunds
         *
         * @description
         * Calculates total cost of all supplemental sources of funds.
         *
         * @return {Number} the total cost of all supplemental sources of funds
         */
        function calculateTotalSupplementalFunds() {
            return calculateTotalFunds();
        }

        function calculateTotalFunds() {
            var totalSupplementalFund = 0;
            if (vm.otherSourcesOfFunds.length > 0) {
                angular.forEach(vm.otherSourcesOfFunds, function(fund) {
                    totalSupplementalFund += fund.amount;
                });
            }
            return totalSupplementalFund;
        }
        /**
          * @ngdoc method
          * @methodOf requisition-summary.controller:RequisitionSummaryController
          * @name validateRequisitionCost
          *
          * @description
          * Verify if the amount required to order commodities is more the the allocated budget.
          *
          * @return {Boolean} false of the budget allocated exceed the amount total requisition cost.
          */
        function validateRequisitionCost() {

            var valid = false;
            if (vm.budgetAmount > 0) {
                if (vm.budgetAmount < this.calculateTotalCost()) {
                    valid = true;
                } else {
                    valid = false;
                }
            }
            broadCastValidRequisitionCoast(valid);

            return valid;
        }

        function broadCastValidRequisitionCoast(valid) {
            $rootScope.$broadcast('validRequisitionCoast', valid);
        }

    }

})();
