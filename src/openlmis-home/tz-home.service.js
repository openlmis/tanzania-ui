/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name openlmis-home.homeService
     *
     * @description
     * Responsible for retrieving all reporting rate information from the server.
     */
    angular
        .module('openlmis-home')
        .service('tzHomeService', service);

    service.$inject = ['$q', '$resource', 'openlmisUrlFactory', 'periodService'];

    function service($q, $resource, openlmisUrlFactory, periodService) {

        var resource = $resource(openlmisUrlFactory('/api/gis/reportingRate/:id'), {}, {

            getLeafLetData: {
                url: openlmisUrlFactory('/api/gis/reportingRate/:program/:period'),
                method: 'GET',
                isArray: true
            },

            getFacilityData: {
                url: openlmisUrlFactory('/api/gis/reportingFacilities/:program/:period/:zone'),
                method: 'GET',
                isArray: true
            }
        });
        var service = {
            getLeafLetData: getLeafLetData,
            getProcessingPeriods: getProcessingPeriods,
            getFacilityData: getFacilityData
        };
        return service;

        /**
         * @ngdoc method
         * @name getLeafLetData
         * @methodOf openlmis-home.homeService
         *
         * @description
         * get leaflet data by specific program and period.
         *
         * @param  {Object}  program program param.
         * @param  {Object}  period period param.
         * @return {Promise}        leaflet Data
         */
        function getLeafLetData(program, period) {
            return resource.getLeafLetData({
                program: program,
                period: period
            }).$promise;
        }

        function getProcessingPeriods(program) {
            var params = {};
            if (program) {
                params.programId = program.id;
            }

            return periodService.query(params);
        }

        /**
         * @ngdoc method
         * @name getFacilityData
         * @methodOf openlmis-home.homeService
         *
         * @description
         * get leaflet data by specific program and period.
         *
         * @param  {Object}  program program param.
         * @param  {Object}  period period param.
         * @param  {Object}  zone zone param.
         * @return {Promise}        leaflet Data
         */
        function getFacilityData(program, period,
                                 zone) {
            return resource.getFacilityData({
                program: program,
                period: period,
                zone: zone
            }).$promise;
        }
    }
})();
