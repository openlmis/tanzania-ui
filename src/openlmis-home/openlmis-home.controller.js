/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name openlmis-home.controller:HomeController
     *
     * @description
     * Controller for managing reporting rate screen.
     */
    angular
        .module('openlmis-home')
        .controller('HomeController', controller);

    controller.$inject = ['reportingRateModalService', '$timeout',  '$q', '$scope', '$state', '$stateParams',
        'leafletData', 'tzHomeService', '$window',
        'programs', 'processingSchedules', 'YEAR_SERVICES', 'loadingModalService', 'indicatorTypes'];

    function controller(reportingRateModalService, $timeout, $q, $scope, $state, $stateParams, leafletData,
                        tzHomeService, $window,
                        programs, processingSchedules, YEAR_SERVICES, loadingModalService,
                        indicatorTypes, viewOptions) {
        var vm = this;
        initiateMap($scope);
        vm.$onInit = onInit;
        vm.openFacilityDetails = openFacilityDetails;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name years
         * @type {Array}
         *
         * @description
         * The list of all years available.
         */
        vm.years = undefined;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name periods
         * @type {Array}
         *
         * @description
         * The list of all filtered periods for a given year.
         */
        vm.periods = undefined;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name schedules
         * @type {Array}
         *
         * @description
         * The list of processing schedules.
         */
        vm.schedules = undefined;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name programs
         * @type {Array}
         *
         * @description
         * The list of all programs available.
         */
        vm.programs = undefined;

        /**
         * @ngdoc property
         * @propertyOf  openlmis-home.controller:HomeController
         * @name processingPeriods
         * @type {List}
         *
         * @description
         * The list of all periods displayed in the table.
         */
        vm.processingPeriods = undefined;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name leafletData
         * @type {Array}
         *
         * @description
         * Contains filtered leaflet Data.
         */
        vm.leafletData = undefined;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name features
         * @type {Array}
         *
         * @description
         * Contains filtered leaflet Data.
         */
        vm.features = undefined;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name features
         * @type {Array}
         *
         * @description
         * Contains filtered leaflet Data.
         */
        vm.indicatorTypes = undefined;

        /**
         * @ngdoc property
         * @propertyOf openlmis-home.controller:HomeController
         * @name features
         * @type {Array}
         *
         * @description
         * Contains filtered leaflet Data.
         */
        vm.viewOptins = undefined;

        function setDefault(programs, processingSchedules, YEAR_SERVICES, indicatorTypes) {

            vm.program = searchByCode(programs, 'ilshosp');
            vm.schedule = searchByCode(processingSchedules, 'groupA');
            vm.year = YEAR_SERVICES[YEAR_SERVICES.length - 1];
            vm.filter.indicatorType = indicatorTypes[2];
            //loadProcessingPeriods();
            filterPeriods();

            $timeout(function() {
                fetchAndProcessLeafletData(vm.program.id, vm.period.id);
            }, 1000);

        }

        /**
         * @ngdoc method
         * @methodOf openlmis-home.controller:HomeController
         * @name $onInit
         *
         * @description
         * Method that is executed on initiating HomeController.
         */
        function onInit() {
            //Filters
            vm.programs = programs;
            vm.years = YEAR_SERVICES;
            vm.schedules = processingSchedules;
            vm.processingPeriods = [];
            vm.periods = [];

            $scope.geojson = {};

            vm.leafletData = leafletData;
            vm.filter = {};
            vm.indicatorTypes = indicatorTypes;
            vm.defaultIndicator = 'period_over_expected';
            vm.filter.indicatorType = vm.defaultIndicator;
            vm.viewOptions = viewOptions;
            setDefault(programs, processingSchedules, YEAR_SERVICES, indicatorTypes);
        }

        vm.expectedFilter = function(item) {
            return item.expected > 0;
        };

        vm.style = function(feature) {

            if (vm.filter !== undefined
                && vm.filter.indicatorType !== undefined) {
                vm.indicatorType = vm.filter.indicatorType;
            } else {
                vm.indicatorType = vm.defaultIndicator;
            }
            var color = (vm.indicatorType === 'ever_over_total') ?
                interpolate(feature.ever, feature.total) :
                (vm.indicatorType === 'ever_over_expected') ?
                    interpolate(feature.ever, feature.expected) :
                    interpolate(feature.period, feature.expected);

            return {
                fillColor: color,
                weight: 1,
                opacity: 1,
                color: 'white',
                dashArray: '1',
                fillOpacity: 0.7
            };
        };

        $scope.drawMap = function(json) {
            if ($scope.$$phase === null || $scope.$$phase === undefined) {
                $scope.$apply(function() {
                    angular.extend($scope, {
                        geojson: {
                            data: json,
                            style: vm.style,
                            onEachFeature: onEachFeature,
                            resetStyleOnMouseout: true
                        }
                    });
                });
            } else {
                angular.extend($scope, {
                    geojson: {
                        data: json,
                        style: vm.style,
                        onEachFeature: onEachFeature,
                        resetStyleOnMouseout: true
                    }
                });
            }
        };

        vm.OnFilterChanged = function() {

            var program, period;

            program = vm.program.id;
            period = vm.period.id;

            fetchAndProcessLeafletData(program, period);

        };

        function fetchAndProcessLeafletData(program, period) {
            tzHomeService.getLeafLetData(program, period)
                .then(function(data) {
                    $scope.features = data;
                    vm.features = data;

                    angular.forEach(vm.features, function(feature) {
                        // eslint-disable-next-line camelcase
                        feature.geometry_text = feature.geometry;
                        feature.geometry = JSON.parse(feature.geometry);
                        feature.type = 'Feature';
                        feature.properties = {};
                        feature.properties.name = feature.name;
                        feature.properties.id = feature.id;
                    });

                    $scope.drawMap({
                        type: 'FeatureCollection',
                        features: $scope.features
                    });
                    zoomAndCenterMap(leafletData, $scope);
                });
        }

        function interpolate(value, count) {
            var d3 = $window.d3;
            var val = parseFloat(value) / parseFloat(count);
            var colorScale = d3.scaleLinear()
                .domain([0, 0.25, 0.5, 0.75, 1])
                .range(['red', 'yellow', 'green'])
                .clamp(true);

            if (isNaN(val)) {
                return 'black';
            }

            return colorScale(val);

        }

        function onEachFeature(feature, layer) {
            layer.bindPopup(popupFormat(feature));
        }

        function popupFormat(feature) {
            return '<table class="table table-bordered" style="width: 250px"><tr><th colspan="2"><b>'
                + feature.properties.name + '</b></th></tr>' +
                '<tr><td>Expected Facilities</td><td class="number">' + feature.expected + '</td></tr>' +
                '<tr><td>Reported This Period</td><td class="number">' + feature.period + '</td></tr>' +
                '<tr><td>Ever Reported</td><td class="number">' + feature.ever + '</td></tr>' +
                '<tr><td class="bold">Total Facilities</b></td><td class="number bold">' + feature.total + '</td></tr>';
        }

        function initiateMap(scope) {
            angular.extend(scope, {
                layers: {
                    baselayers: {
                        xyz: {
                            name: 'OpenStreetMap (XYZ)',
                            url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            type: 'xyz'
                        }
                    }
                },
                legend: {
                    position: 'bottomleft',
                    colors: ['#FF0000', '#FFFF00', '#5eb95e', '#000000'],
                    labels: ['Non Reporting', 'Partial Reporting ', 'Fully Reporting', 'Not expected to Report'],
                    legend: {
                        'Non Reporting': '#FF0000',
                        'Partial Reporting': '#FFFF00',
                        'Fully Reporting': '#5eb95e',
                        'Not expected to Report': '#000000'
                    }
                }
            });

        }

        /* global L */
        function zoomAndCenterMap(leafletData, $scope) {
            leafletData.getMap().then(function(map) {
                var latlngs = [];
                for (var c = 0; c < $scope.features.length; c++) {
                    if ($scope.features[c].geometry === null
                        || angular.isUndefined($scope.features[c].geometry)) {
                        continue;
                    }

                    if ($scope.features[c].geometry.coordinates === null
                        || angular.isUndefined($scope.features[c].geometry.coordinates)) {
                        continue;
                    }

                    for (var i = 0; i < $scope.features[c].geometry.coordinates.length; i++) {
                        var coord = $scope.features[c].geometry.coordinates[i];
                        for (var j = 0; j < coord.length; j++) {
                            var points = coord[j];
                            for (var p in points) {
                                var latlng;
                                if (angular.isNumber(points[p])) {
                                    latlng = L.GeoJSON.coordsToLatLng(points);
                                } else {
                                    latlng = L.GeoJSON.coordsToLatLng(points[p]);
                                }
                                latlngs.push(latlng);
                            }
                        }
                    }
                }

                map.fitBounds(latlngs);
            });

        }
        function filterPeriods() {
            vm.periods = [];
            if (vm.processingPeriods) {
                vm.periods = vm.processingPeriods.filter(
                    function(period) {

                        var matchYear = true;
                        if (vm.year) {
                            var filteredYear = new Date(period.startDate).getFullYear();
                            matchYear = parseInt(filteredYear, 10) === parseInt(vm.year.name, 10);
                        }

                        var matchSchedule = true;
                        if (vm.schedule) {
                            matchSchedule = period.processingSchedule.id === vm.schedule.id;
                        }

                        return matchYear && matchSchedule;

                    }
                );
            }
            vm.period = vm.periods[0];
        }

        $scope.$watch('vm.year', function() {
            filterPeriods();
        });

        $scope.$watch('vm.schedule', function() {
            filterPeriods();
        });

        function loadProcessingPeriods() {
            return tzHomeService.getProcessingPeriods(vm.program)
                .then(function(periods) {
                    vm.processingPeriods = periods.content;
                    vm.periods = periods.content;

                    filterPeriods();
                });
        }

        $scope.$watch('vm.program', function() {
            loadingModalService.open();
            loadProcessingPeriods().then(function() {
                loadingModalService.close();
            });
        });

        $scope.exportReport = function() {
            // Assuming vm.features is your data array
            var data = vm.features;

            // Prepare CSV data with header
            var csvData = [['District', 'Expected', 'Reported', '% Reported']];

            // Calculate percentage and add to each data item
            angular.forEach(data, function(item) {
                if (item.expected > 0) {
                    var percentage = ((item.period / item.expected) * 100)
                        .toFixed(0) + ' %';
                    csvData.push([item.name, item.expected, item.period, percentage]);
                }
            });

            // Export only if there is valid data
            if (csvData.length > 1) {
                // Generate CSV file
                var blob = new Blob([csvData.map(function(row) {
                    return row.join(',');
                }).join('\n')], {
                    type: 'text/csv;charset=utf-8;'
                });
                var csvFile = window.URL.createObjectURL(blob);

                // Create an anchor element
                var link = document.createElement('a');
                link.href = csvFile;
                link.target = '_blank';
                // Open in a new tab/window, or remove this line to open in the same tab/window
                link.download = 'report.csv';

                // Trigger download
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            } else {
                // No valid data to export
                alert('No data with expected > 0 to export.');
            }
        };

        function openFacilityDetails(feature) {
            var zone = feature.id;
            var program = vm.program.id;
            var period = vm.period.id;
            loadFacilityDetailsModal(feature.name, program, period, zone);
        }

        /**
         * @ngdoc method
         * @methodOf openlmis-home.controller:HomeController
         * @name loadRejectionReasonModal
         *
         * @description
         * Display facility details dialog.
         */

        function loadFacilityDetailsModal(district, program, period, zone) {
            var deferred = $q.defer();

            reportingRateModalService.open(tzHomeService, program, period, zone, district)
                .then(function(rejectionReasons) {
                    deferred.resolve(rejectionReasons);
                })
                .catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function searchByCode(data, code) {
            for (var i = 0; i < data.length; i++) {
                if (data[i].code === code) {
                    return data[i];
                }
            }
            return null;
        }

    }
})();
