/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('openlmis-home')
        .config(routes);

    routes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routes($stateProvider, $urlRouterProvider) {

        $stateProvider.state('openlmis.home', {
            url: '/home',
            // keep home menu always at the first place
            priority: 999,
            showInNavigation: true,
            label: 'openlmisHome.home',
            isOffline: true,
            views: {
                '@': {
                    controller: 'HomeController',
                    templateUrl: 'openlmis-home/reporting-rate.html',
                    controllerAs: 'vm',
                    resolve: {
                        indicatorTypes: function() {

                            var indicatorTypes = [
                                {
                                    code: 'ever_over_total',
                                    name: 'Ever Reported / Total Facilities'
                                },
                                {
                                    code: 'ever_over_expected',
                                    name: 'Ever Reported / Expected Facilities'
                                },
                                {
                                    code: 'period_over_expected',
                                    name: 'Reported during period / Expected Facilities'
                                }
                            ];
                            return indicatorTypes;
                        },
                        viewOptions: function() {

                            var viewOptions = [
                                {
                                    id: '0',
                                    name: 'Non Reporting Only'
                                },
                                {
                                    id: '1',
                                    name: 'Reporting Only'
                                },
                                {
                                    id: '2',
                                    name: 'All'
                                }
                            ];
                            return viewOptions;

                        },
                        programs: function(programService) {
                            return programService.getAll();
                        },
                        processingSchedules: function($q, processingScheduleService) {
                            var deferred = $q.defer();

                            processingScheduleService.query({}).then(
                                function(response) {
                                    deferred.resolve(response.content);
                                },
                                deferred.reject
                            );
                            return deferred.promise;
                        }
                    }
                }
            }
        });

        $urlRouterProvider
            .when('', '/home')
            .when('/', '/home');

    }

})();