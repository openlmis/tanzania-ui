/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name openlmis-home.reportingRateModalService
     *
     * @description
     * Responsible for handling requisition budget modal.
     */
    angular
        .module('openlmis-home')
        .service('reportingRateModalService', reportingRateModalService);

    reportingRateModalService.$inject = ['openlmisModalService'];

    function reportingRateModalService(openlmisModalService) {
        this.open = open;

        /**
         * @ngdoc method
         * @methodOf openlmis-home.reportingRateModalService
         * @name open
         *
         * @description
         * Opens the reporting rate modal.
         *
         * @return {Promise}
         */
        function open(tzHomeService, program, period, zone, district) {

            return openlmisModalService.createDialog({
                templateUrl: 'openlmis-home/facility-list-modal.html',
                backdrop: 'static',
                controllerAs: 'vm',
                show: true,
                controller: function(facilitiesData, modalDeferred) {
                    var vm = this;
                    vm.closeModal = closeModal;
                    vm.$onInit = onInit;

                    function onInit() {

                        vm.districtName = district;

                        vm.facilitiesData = facilitiesData;
                    }

                    function closeModal() {
                        modalDeferred.resolve();
                    }

                },
                resolve: {

                    facilitiesData: function() {

                        return tzHomeService.getFacilityData(program, period, zone)
                            .then(function(response) {

                                return response;

                            });
                    }
                }
            }).promise;
        }
    }
})();
