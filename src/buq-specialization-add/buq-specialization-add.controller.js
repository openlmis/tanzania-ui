/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */
(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name buq-specialization-add.controller:BuqSpecializationAddController
     *
     * @description
     * Exposes method for creating/updating buqSpecialization to the modal view.
     */
    angular
        .module('buq-specialization-add')
        .controller('BuqSpecializationAddController', controller);

    controller.$inject =
        ['buqSpecializations', 'buqSpecialization', 'BuqSpecialization', 'buqSpecializationService',
            'stateTrackerService', 'loadingModalService', 'notificationService'];

    function controller(buqSpecializations, buqSpecialization,
                        BuqSpecialization, buqSpecializationService,
                        stateTrackerService, loadingModalService, notificationService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.save = save;
        vm.validateBuqSpecializationName = validateBuqSpecializationName;

        /**
         * @ngdoc property
         * @propertyOf buq-specialization-add.controller:BuqSpecializationAddController
         * @type {BuqSpecialization}
         * @name buqSpecialization
         *
         * @description
         * BuqSpecialization that is being created.
         */
        vm.buqSpecialization = undefined;

        /**
         * @ngdoc method
         * @methodOf buq-specialization-add.controller:BuqSpecializationAddController
         * @name $onInit
         *
         * @description
         * Initialization method of the BuqSpecializationAddController.
         */
        function onInit() {
            vm.buqSpecialization = buqSpecialization;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-add.controller:BuqSpecializationAddController
         * @name validateBuqSpecializationName
         *
         * @description
         * Validates the entered buqSpecialization name.
         *
         * @return {String} the error message key, undefined if buqSpecialization is valid
         */
        function validateBuqSpecializationName() {
            if (isBuqSpecializationNameDuplicated()) {
                return 'buqSpecializationAdd.buqSpecializationNameDuplicated';
            }
        }

        function isBuqSpecializationNameDuplicated() {
            if (!vm.buqSpecialization || !vm.buqSpecialization.name) {
                return false;
            }

            return buqSpecializations.filter(function(buqSpecialization) {
                return (
                    (buqSpecialization.name.toUpperCase()
                     === vm.buqSpecialization.name.toUpperCase())
                    && buqSpecialization.id !== vm.buqSpecialization.id
                );
            }).length;
        }

        /**
         * @ngdoc method
         * @methodOf buq-specialization-add.controller:BuqSpecializationAddController
         * @name save
         *
         * @description
         * Saves the specialization and takes user back to the previous state.
         */
        function save() {
            loadingModalService.open();
            return buqSpecializationService.save(vm.buqSpecialization)
                .then(function(specialization) {
                    notificationService
                        .success('buqSpecializationAdd.buqSpecializationSavedSuccessfully');
                    stateTrackerService.goToPreviousState();
                    return specialization;
                })
                .catch(function() {
                    notificationService.error('buqSpecializationAdd.buqSpecializationFailedToSave');
                    loadingModalService.close();
                });
        }

    }
})();