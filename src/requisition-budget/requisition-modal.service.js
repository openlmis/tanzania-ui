/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name requisition-budget.requisitionBudgetModalService
     *
     * @description
     * Responsible for handling requisition budget modal.
     */
    angular
        .module('requisition-budget')
        .service('requisitionBudgetModalService', requisitionBudgetModalService);

    requisitionBudgetModalService.$inject = ['openlmisModalService', '$filter', 'notificationService'];

    function requisitionBudgetModalService(openlmisModalService, $filter) {
        this.open = open;

        /**
         * @ngdoc method
         * @methodOf requisition-budget.requisitionBudgetModalService
         * @name open
         *
         * @description
         * Opens the requisition budget modal.
         *
         * @return {Promise}
         */
        function open(vm, requisitionBudgetService, oldValue) {

            return openlmisModalService.createDialog({
                templateUrl: 'requisition-view/budget-modal.html',
                backdrop: 'static',
                controllerAs: 'vm',
                show: true,
                controller: function(sourceOfFunds, modalDeferred, requisitionBudgetService, notificationService) {
                    var requisitionBudgetObject = {};
                    this.selectedSourceOfFunds = [];
                    this.addSourceOfFund = addSourceOfFund;
                    this.removeBudgetRequisition = removeBudgetRequisition;
                    this.cancel = cancel;
                    this.sourceOfFunds = sourceOfFunds;
                    this.save = save;
                    this.totalSupplementalFunds = calculateTotalSupplementalFunds;

                    if (oldValue !== undefined) {
                        this.selectedSourceOfFunds = oldValue;
                    }

                    function addSourceOfFund() {
                        this.sourceOfFund.selected = true;
                        this.selectedSourceOfFunds.push(this.sourceOfFund);
                        requisitionBudgetObject.requisitionId = vm.requisition.id;
                        requisitionBudgetObject.sourceOfFundId = this.sourceOfFund.sourceOfFundId;
                        requisitionBudgetObject.budgetAmount = this.sourceOfFund.amount;
                        requisitionBudgetObject.id = this.sourceOfFund.id;
                        requisitionBudgetService.update(requisitionBudgetObject)
                            .then(function(response) {
                                notificationService.success(
                                    response.sourceOfFundName +
                                    'requisitionSummary.requisition.fund.source.updated.success'
                                );

                            }, function(error) {
                                notificationService.error(error);
                            });

                        this.sourceOfFund = {};
                        this.sourceOfFund.amount = 0;
                    }

                    function removeBudgetRequisition(sourceOfFund) {

                        var index = this.selectedSourceOfFunds.indexOf(sourceOfFund);
                        if (index > -1) {
                            this.selectedSourceOfFunds.splice(index, 1);
                        }
                    }

                    function cancel() {
                        modalDeferred.reject();
                    }

                    function save() {
                        oldValue = undefined;
                        modalDeferred.resolve(this.selectedSourceOfFunds);
                    }

                    function calculateTotalSupplementalFunds() {

                        var total = 0;
                        angular.forEach(this.selectedSourceOfFunds, function(sourceOfFund) {
                            total += sourceOfFund.amount;
                        });

                        return total;
                    }

                },
                resolve: {

                    sourceOfFunds: function(requisitionBudgetService) {

                        return requisitionBudgetService.getByFacilityId(vm.requisition.facility.id)
                            .then(function(response) {
                                var funds = [];

                                funds = $filter('filter')(response.data, function(fund) {
                                    return fund.sourceOfFundName.toLowerCase() !== 'msd';
                                });

                                return funds;

                            });
                    }
                }
            }).promise;
        }
    }
})();
