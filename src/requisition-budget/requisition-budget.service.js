/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc service
     * @name budget.budgetService
     *
     * @description
     * Responsible for retrieving BUDGET from the server.
     */
    angular
        .module('requisition-budget')
        .factory('requisitionBudgetService', service);

    service.$inject = ['budgetUrlFactory', '$resource', 'FormData', '$q', '$http'];

    function service(budgetUrlFactory, $resource, FormData, $q, $http) {

        var resource = $resource(budgetUrlFactory('/api/requisitionBudgets/:id'), {}, {
            update: {
                method: 'PUT'
            }
        });

        return {
            getByFacilityId: getByFacilityId,
            update: update
        };

        /**
         * @ngdoc method
         * @methodOf budget.budgetService
         * @name get
         *
         * @description
         * Gets Budgets by facility id.
         *
         * @param  {String}  facilityId facility UUID
         * @return {Promise}    Budget info
         */
        function getByFacilityId(facilityId) {
            var factory = budgetUrlFactory('/api/budgets/' + facilityId + '/budgets');
            return $http.get(factory, {});
        }

        /**
         * @ngdoc method
         * @methodOf budget.budgetService
         * @name update
         *
         * @description
         * Updates Requisition budget.
         *
         * @param  {Object}  requisition budget to be updated
         * @return {Promise} Updated requisition budget
         */
        function update(requisitionBudget) {
            return resource.update({
                id: requisitionBudget.id
            }, requisitionBudget).$promise;
        }

    }
})();
