apk update
apk upgrade
apk add curl bash gcc make musl musl-dev python3 python3-dev libffi libffi-dev openssl openssl-dev cargo
curl -O https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
pip install --upgrade pip setuptools build wheel
pip install "Cython<3.0" "pyyaml<6" --no-build-isolation
pip install --upgrade docker-py
##pip install docker==6.1.3 docker-compose --no-cache-dir
curl -L "https://github.com/docker/compose/releases/download/v2.20.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker --version
docker-compose --version