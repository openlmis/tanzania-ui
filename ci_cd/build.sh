export VERSION=$(grep "version" project.properties|cut -d'=' -f2)
curl -o .env -L https://raw.githubusercontent.com/OpenLMIS/openlmis-ref-distro/master/settings-sample.env
docker-compose run --entrypoint /dev-ui/build.sh tanzania-ui
docker-compose build image
docker tag openlmistz/tanzania-ui:latest openlmistz/tanzania-ui:$VERSION
docker login -u $cidockeruser -p $cidockerpassword
docker push openlmistz/tanzania-ui:$VERSION
docker logout