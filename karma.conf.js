module.exports = function(config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        captureTimeout: 210000,
        browserDisconnectTolerance: 3,
        browserDisconnectTimeout: 210000,
        browserNoActivityTimeout: 210000,
    });
};