export VERSION=$(grep "version" project.properties|cut -d'=' -f2)

mv .env .env_backup
curl -o .env -L https://raw.githubusercontent.com/OpenLMIS/openlmis-ref-distro/master/settings-sample.env

docker-compose pull
docker-compose run --entrypoint /dev-ui/build-no-test.sh tanzania-ui

docker-compose build image
docker tag openlmistz/tanzania-ui:latest openlmistz/tanzania-ui:$VERSION
docker logout
echo "******** LOGIN TO OPENLMIS DOCKER HUB FOR PUSHING IMAGE ********"
docker login
docker push openlmistz/tanzania-ui:$VERSION
docker logout
mv .env_backup .env